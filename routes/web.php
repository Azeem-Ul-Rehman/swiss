<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/cache', function () {

    \Illuminate\Support\Facades\Artisan::call('key:generate');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Artisan::call('route:clear');

    return 'Commands run successfully Cleared.';
});
Route::get('/', function () {
    $investorCount = \App\User::where('user_type', 'investor')->count();
    $startupCount = \App\User::where('user_type', 'startup')->count();
    return view('welcome', compact('investorCount', 'startupCount'));

})->name('index');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

use App\Http\Controllers\StripeController;
use App\Http\Middleware\CheckPayment;

Route::middleware([CheckPayment::class])->group(function () {
    Route::get('investor', 'InvestorController@index')->name('investor.index');
    Route::get('/edit/investor/{id}', 'InvestorController@edit')->name('investor.edit');
    Route::post('/edit/investor/update', 'InvestorController@update')->name('investor.update');

    Route::get('/investor-message-inbox/{id}', 'InvestorController@viewMessage')->name('page.index');
    Route::get('/message', 'InvestorController@getMessage')->name('get-messages');
    Route::post('message', 'InvestorController@sendMessage')->name('store-messages');
    Route::get('/investor-message-all', 'InvestorController@getAllMessage')->name('investor-message-all.index');

    Route::get('startup', 'StartUpController@index')->name('startup.index');
    Route::get('/startup/create', 'StartUpController@create')->name('startup.create');
    Route::post('/startup/store', 'StartUpController@store')->name('startup.store');

    Route::get('/startup/profile/{id}', 'StartUpController@editProfile')->name('startup.profile');
    Route::post('/startup/profile/update', 'StartUpController@updateProfile')->name('startup.profile.update');


    Route::get('/discover', 'StartUpController@discover')->name('discover');
    Route::get('/startup/detail/{id}', 'StartUpController@startupDetail')->name('startup.detail');


    Route::get('/startup/edit/{id}', 'StartUpController@edit')->name('startup.edit');
    Route::post('/startup/update', 'StartUpController@update')->name('startup.update');
    Route::get('/startup/delete/{id}', 'StartUpController@destroy')->name('startup.delete');


//Route::get('/startup-message-all/{id}', 'StartUpController@viewMessage')->name('startup-get.index');
    Route::get('/startup-message-all', 'StartUpController@viewMessage')->name('startup-get.index');

    Route::get('/startup-message-get', 'StartUpController@getMessage')->name('startup-get-messages');
    Route::post('startup--store-message-guide', 'StartUpController@sendMessage')->name('startup-store-messages');
    Route::get('cancel/subscription', 'StartUpController@cancelSubscription')->name('cancel.subscription');

});


Route::get('payment', [StripeController::class, 'stripe'])->name('payment.index');
Route::post('payment/store', [StripeController::class, 'stripePost'])->name('payment.post');

Route::get('forget-password', 'AccountController@getEmail');
Route::post('forget-password', 'AccountController@postEmail');
Route::get('reset-password/{token}', 'AccountController@getPassword');
Route::post('reset-password', 'AccountController@updatePassword');


Route::middleware(['auth'])->group(function () {
    Route::prefix('admin')->name('admin.')->group(function () {
        Route::resource('/dashboard', 'Backend\DashboardController');
        Route::resource('/investors', 'Backend\InvestorController');
        Route::resource('/startups', 'Backend\StartUpController');
        Route::resource('/settings', 'Backend\SettingController', [
            'only' => ['index', 'update']
        ]);
        Route::resource('/contacts', 'Backend\ContactUsController', [
            'only' => ['index', 'destroy']
        ]);
    });
});
Route::resource('/contacts', 'Backend\ContactUsController', [
    'only' => ['create', 'store']
]);

//Ajax Routes
Route::prefix('ajax')->name('ajax.')->group(function () {
    Route::get('delete/startup/image', 'Backend\AjaxController@deleteStartupImage')->name('update.remove.image');
    Route::get('delete/startup/video', 'Backend\AjaxController@deleteStartupVideo')->name('update.remove.video');
});

