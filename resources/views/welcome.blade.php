<!DOCTYPE html>
<html lang="en">
<head>
    <title>Startup Web</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

    <!-- Owl Carousel Min CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
          integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
          crossorigin="anonymous"/>
    <!-- Own Carousel Min Theme CSS -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"
          integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw=="
          crossorigin="anonymous"/>
    <!-- Fontawesome 4.7.0 Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
    <style>
        #our-team-section-count {
            margin-top: -95px;
        }

        .column .card {
            background: #2b3a5e;
        }

        .white-color {
            color: #fff;
        }

        .count-section {
            margin-top: 14px;
            font-size: 35px;
        }

        .text-center {
            text-align: center;
        }

        .privacy {
            padding: 13px;
            color: #fff;
            text-align: center;
        }

        /* Responsive columns */
        @media screen and (max-width: 600px) {
            .column {
                width: 100%;
                display: block;
                margin-bottom: 10px;
            }

            #our-team-section-count {
                margin-top: -41px;
            }
        }
    </style>
</head>
<body>
@if($errors->any())
    <div class="alert alert-danger">
        {{ implode('&', $errors->all(':message!! ')) }}
    </div>
@endif

<header class="m-0">
    <div class="container">
        <div class="row">
            <div class="col-12 p-0">
                <nav class="navbar navbar-light bg-light navbar-expand-md">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>

                    </button>
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                        <a class="navbar-brand" href="{{ route('index') }}"><img
                                src="{{ asset('assets/imgs/main-logo-3.png') }}"
                                width="170" alt="company logo"/></a>
                        <ul class="navbar-nav ml-auto mt-2 mt-md-0 mt-lg-0">
                            <li class="nav-item active">
                                <a class="nav-link px-md-0 px-lg-2" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link px-md-0 px-lg-2" href="#about-us-section">About us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link px-md-0 px-lg-2" href="#our-team-section">Our team</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link px-md-0 px-lg-2" href="#contact-us-section">Contact us</a>
                            </li>
                            @auth
                                @if(Auth::user()->user_type == 'investor')
                                    <li class="nav-item">
                                        <a class="nav-link px-md-0 px-lg-2" href="{{ route('investor.index') }}">My
                                            account</a>
                                    </li>
                                @endif
                                @if(Auth::user()->user_type == 'startup')
                                    <li class="nav-item">
                                        <a class="nav-link px-md-0 px-lg-2" href="{{ route('startup.index') }}">My
                                            account</a>
                                    </li>
                                @endif
                            @endauth
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link px-md-0 px-lg-2" href="#" data-toggle="modal"
                                       data-target="#loginModal">Login</a>
                                </li>
                            @endguest
                        </ul>
                        @guest
                            <div class="form-inline my-2 my-lg-0 mr-3 position-relative">
                                <button class="btn btn-default my-2 my-sm-0 design-button" data-toggle="modal"
                                        data-target="#startupsRegisterModal">
                                    <span class="design-button-span"  style="z-index:0 !important; ">Startup</span>
                                </button>
                            </div>
                            <div class="form-inline my-2 my-lg-0 position-relative">
                                <button class="btn btn-default my-2 my-sm-0 design-button" data-toggle="modal"
                                        data-target="#investorRegisterModal">
                                    <span class="design-button-span" style="z-index:0 !important; ">Investor</span>
                                </button>
                            </div>
                        @endguest
                        @auth
                            <div class="form-inline my-2 my-lg-0 mr-3 position-relative">
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <button class="btn btn-default my-2 my-sm-0 design-button">
                                        <span class="design-button-span">Logout</span>

                                    </button>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        @endauth
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
<section id="hero-slider-section">
    <div class="container-fluid p-0">
        <div class="col-12 p-0">
            <div class="row m-0">
                <div class="img-sliders-wrapper owl-carousel owl-theme">
                    <div class="slide">
                        <div class="img-item">
                            <img src="{{ asset('assets/imgs/dentist-slide1.jpg') }}" alt="">
                            <div class="slide-text-box">
                                <p class="slide-heading">
                                    Experience trust,
                                    <br>
                                    and proven success
                                </p>
                                <br>
                                <p class="slide-text">
                                    Fhbbsx nc
                                </p>
                                <br>
                                {{--                                <button class="d-none d-md-block btn btn-default slide-rect">--}}
                                {{--                                    Latest News--}}
                                {{--                                </button>--}}
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <div class="img-item">
                            <img src="{{ asset('assets/imgs/dentist-slide2.jpg') }}" alt="">
                            <div class="slide-text-box">
                                <p class="slide-heading">
                                    Experience trust,
                                    <br>
                                    and proven success
                                </p>
                                <br>
                                <p class="slide-text">
                                    Fhbbsx nc
                                </p>
                                <br>
                                {{--                                <button class="d-none d-md-block btn btn-default slide-rect">--}}
                                {{--                                    Latest News--}}
                                {{--                                </button>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <section id="latest-news-section">
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="container">
                <div class="row">
                    <div class="section-spacing">
                        <div class="col-12">
                            <div class="d-flex flex-column align-items-center justify-content-center">
                                <h3>Latest news</h3>
                                <div class="para-top-spacing"></div>
                                <p class="text-center">
                                    Keep up to date with the latest news in health, including
                                    <br>
                                    recent announcements and new research.
                                </p>
                                <div class="para-bottom-spacing"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <div class="card p-3">
                                        <img class="card-img-top" src="assets/imgs/dentist-blog1-400x250.jpg" alt="Card image cap">
                                        <div class="card-body">
                                          <h5 class="card-title">The clear difference between cold brew and iced coffee and else</h5>
                                          <a href="#" class="">Read more<i class="fa fa-long-arrow-right arrow-icon" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4">
                                    <div class="card p-3">
                                        <img class="card-img-top" src="assets/imgs/dentist-blog2-400x250.jpg" alt="Card image cap">
                                        <div class="card-body">
                                          <h5 class="card-title">The clear difference between cold brew and iced coffee and else</h5>
                                          <a href="#" class="">Read more<i class="fa fa-long-arrow-right arrow-icon" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4">
                                    <div class="card p-3">
                                        <img class="card-img-top" src="assets/imgs/dentist-blog3-400x250.jpg" alt="Card image cap">
                                        <div class="card-body">
                                          <h5 class="card-title">The clear difference between cold brew and iced coffee and else</h5>
                                          <a href="#" class="">Read more<i class="fa fa-long-arrow-right arrow-icon" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<section id="about-us-section">
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="container">
                <div class="row">
                    <div class="section-spacing">
                        <div class="col-12">
                            <div class="row">
                                <div
                                    class="col-12 col-lg-6 d-flex align-items-center justify-content-center mb-4 mb-lg-auto">
                                    <div>
                                        <h3> About us </h3>
                                        <p class="about-us-para about-us-para-opening-sentence">
                                            You’re only 5 minutes away from a beautiful startup.
                                        </p>
                                        <p class="about-us-para">
                                            I want to solve the problem of clean water scarcity in developing
                                            countries. This problem is experienced by people in rural areas
                                            where water supply and treatment infrastructure doesn’t exist. I want
                                            to solve that problem by providing village-scale pumps that would give
                                            every village a way to draw on clean water from underground sources.
                                            This would be a better solution because it would be easy and quick
                                            to install, it would be simple to operate, it would not require large
                                            investments in infrastructure, it would last long, and rural
                                            entrepreneurs could even earn money by selling clean water.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6 d-flex align-items-center justify-content-center">
                                    <div class="about-us-img-wrapper">
                                        <img class="img-fluid" src="{{ asset('assets/imgs/about1.jpg') }}"
                                             alt="startup about us img">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="our-service-section">
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="container">
                <div class="row">
                    <div class="section-spacing mb-0">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 section-main-title mb-4">
                                    <h3 class="text-center"> Our Service </h3>
                                </div>
                                <div class="col-12 service-section-cols">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-12 service-section-col section-col-1">
                                            <div class="service-section-col-card">
                                                <div
                                                    class="service-section-col-icon d-flex align-items-center justify-content-center">
                                                    <img class="img-fluid"
                                                         src="{{ asset('assets/imgs/register-service.png') }}"
                                                         alt="mimo finds service icon 1" width="60">
                                                </div>
                                                <div class="service-section-col-main-title text-center my-4">
                                                    <span>Register</span>
                                                </div>
                                                <div class="service-section-col-main-text text-center">
                                                    <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-12 service-section-col section-col-2 mb-4">
                                            <div class="service-section-col-card">
                                                <div
                                                    class="service-section-col-icon d-flex align-items-center justify-content-center">
                                                    <img class="img-fluid"
                                                         src="{{ asset('assets/imgs/create-profile.png') }}"
                                                         alt="mimo finds service icon 2" width="60">
                                                </div>
                                                <div class="service-section-col-main-title text-center my-4">
                                                    <span>Create your Profile</span>
                                                </div>
                                                <div class="service-section-col-main-text text-center">
                                                    <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-12 service-section-col section-col-3 mb-4">
                                            <div class="service-section-col-card">
                                                <div
                                                    class="service-section-col-icon d-flex align-items-center justify-content-center">
                                                    <img class="img-fluid"
                                                         src="{{ asset('assets/imgs/search-and-find.png') }}"
                                                         alt="mimo finds service icon 3" width="60">
                                                </div>
                                                <div class="service-section-col-main-title text-center my-4">
                                                    <span>Search & Find</span>
                                                </div>
                                                <div class="service-section-col-main-text text-center">
                                                    <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-12 service-section-col section-col-4">
                                            <div class="service-section-col-card">
                                                <div
                                                    class="service-section-col-icon d-flex align-items-center justify-content-center">
                                                    <img class="img-fluid" src="{{ asset('assets/imgs/contact.png') }}"
                                                         alt="mimo finds service icon 4" width="60">
                                                </div>
                                                <div class="service-section-col-main-title text-center my-4">
                                                    <span>Contact</span>
                                                </div>
                                                <div class="service-section-col-main-text text-center">
                                                    <span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="startup-section">
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="container">
                <div class="row">
                    <div class="section-spacing">
                        <div class="col-12">
                            <div class="row web-view-startup">
                                <div class="col-md-6 d-flex align-items-center justify-content-center">
                                    <div class="about-us-img-wrapper">
                                        <img class="img-fluid" src="{{ asset('assets/imgs/about2.jpg') }}"
                                             alt="startup about us img">
                                    </div>
                                </div>
                                <div class="col-md-6 d-flex align-items-center justify-content-center">
                                    <div>
                                        <h3> Startup </h3>
                                        <p class="about-us-para about-us-para-opening-sentence">
                                            You’re only 5 minutes away from a beautiful startup.
                                        </p>
                                        <p class="about-us-para">
                                            I want to solve the problem of clean water scarcity in developing
                                            countries. This problem is experienced by people in rural areas
                                            where water supply and treatment infrastructure doesn’t exist. I want
                                            to solve that problem by providing village-scale pumps that would give
                                            every village a way to draw on clean water from underground sources.
                                            This would be a better solution because it would be easy and quick
                                            to install, it would be simple to operate, it would not require large
                                            investments in infrastructure, it would last long, and rural
                                            entrepreneurs could even earn money by selling clean water.
                                        </p>
                                        @guest
                                            <button class="btn btn-default mt-3 my-2 my-sm-0 design-button"
                                                    data-toggle="modal"
                                                    data-target="#startupsRegisterModal">
                                                <span class="design-button-span">Startup</span>
                                            </button>
                                        @endguest
                                    </div>
                                </div>
                            </div>
                            <div class="row mobile-view-startup my-3">
                                <div class="col-12">
                                    <div>
                                        <h3> Startup </h3>
                                        <p class="about-us-para about-us-para-opening-sentence">
                                            You’re only 5 minutes away from a beautiful startup.
                                        </p>
                                        <p class="about-us-para">
                                            I want to solve the problem of clean water scarcity in developing
                                            countries. This problem is experienced by people in rural areas
                                            where water supply and treatment infrastructure doesn’t exist. I want
                                            to solve that problem by providing village-scale pumps that would give
                                            every village a way to draw on clean water from underground sources.
                                            This would be a better solution because it would be easy and quick
                                            to install, it would be simple to operate, it would not require large
                                            investments in infrastructure, it would last long, and rural
                                            entrepreneurs could even earn money by selling clean water.
                                        </p>
                                        @guest
                                            <button class="btn btn-default mt-3 my-2 my-sm-0 mb-5 design-button"
                                                    data-toggle="modal"
                                                    data-target="#startupsRegisterModal">
                                                <span class="design-button-span">Startup</span>
                                            </button>
                                        @endguest

                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="about-us-img-wrapper">
                                        <img class="img-fluid" src="{{ asset('assets/imgs/about2.jpg') }}"
                                             alt="startup about us img">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="investor-section">
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="container">
                <div class="row">
                    <div class="investor-section-spacing">
                        <div class="col-12">
                            <div class="row">
                                <div
                                    class="col-12 col-lg-6 d-flex align-items-center justify-content-center mt-lg-auto mb-4 mb-lg-auto">
                                    <div>
                                        <h3> Investor </h3>
                                        <p class="about-us-para about-us-para-opening-sentence">
                                            You’re only 5 minutes away from a beautiful startup.
                                        </p>
                                        <p class="about-us-para">
                                            I want to solve the problem of clean water scarcity in developing
                                            countries. This problem is experienced by people in rural areas
                                            where water supply and treatment infrastructure doesn’t exist. I want
                                            to solve that problem by providing village-scale pumps that would give
                                            every village a way to draw on clean water from underground sources.
                                            This would be a better solution because it would be easy and quick
                                            to install, it would be simple to operate, it would not require large
                                            investments in infrastructure, it would last long, and rural
                                            entrepreneurs could even earn money by selling clean water.
                                        </p>
                                        @guest
                                            <button class="btn btn-default my-2 my-sm-0 design-button"
                                                    data-toggle="modal"
                                                    data-target="#investorRegisterModal">
                                                <span class="design-button-span">Investor</span>
                                            </button>
                                        @endguest
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6 d-flex align-items-center justify-content-center">
                                    <div class="about-us-img-wrapper">
                                        <img class="img-fluid" src="{{ asset('assets/imgs/about3.jpg') }}"
                                             alt="startup about us img">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="our-team-section">
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="container">
                <div class="row">
                    <div class="section-spacing">
                        <div class="col-12">
                            <div class="d-flex flex-column align-items-center justify-content-center">
                                <h3>Our team</h3>
                                <div class="para-top-spacing"></div>
                                <p class="text-center">
                                    Our people are our greatest asset. Our ability to deliver outstanding results for
                                    <br>
                                    our clients starts with our team of smart and capable experts.
                                </p>
                                <div class="para-bottom-spacing"></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-md-6 mb-5 mb-md-0">
                                    <div class="card p-3">
                                        <img class="card-img-top" src="{{ asset('assets/imgs/dentist9.jpg') }}"
                                             alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title">Fabien </h5>
                                            <p class="job-title">Marketing Manager</p>
                                            <p class="person-bio">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing
                                                <br>
                                                elit, sed do eiusmod tempor.
                                            </p>
                                            <div
                                                class="team-social-icons-wrapper d-flex flex-row align-items-center justify-content-between">
                                                    <span>
                                                        <a href="mailto:fabien.baumann@mimofind.ch">
                                                            <span class="team-social-icons email-icon text-center">
                                                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                                            </span>
                                                        </a>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mt-0 mt-sm-3 mt-md-0">
                                    <div class="card p-3">
                                        <img class="card-img-top" src="{{ asset('assets/imgs/dentist10.jpg') }}"
                                             alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title">Jozef</h5>
                                            <p class="job-title">Marketing Manager</p>
                                            <p class="person-bio">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing
                                                <br>
                                                elit, sed do eiusmod tempor.
                                            </p>
                                            <div
                                                class="team-social-icons-wrapper d-flex flex-row align-items-center justify-content-between">
                                                    <span>
                                                        <a href="mailto:jozef.ceni@mimofind.ch">
                                                            <span class="team-social-icons email-icon text-center">
                                                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                                            </span>
                                                        </a>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="our-team-section-count">

    <div class="container">

        <div class="section-spacing">
            <div class="row">
                <div class="col-md-6">
                    <div class="column">
                        <div class="card text-center">
                            <p class="count-section white-color"><i class="fa fa-user"></i></p>
                            <h3 class="white-color">{{$investorCount}}</h3>
                            <p class="white-color">Investors</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="column">
                        <div class="card text-center">
                            <p class="count-section white-color"><i class="fa fa-user"></i></p>
                            <h3 class="white-color">{{$startupCount}}</h3>
                            <p class="white-color">Startups</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="contact-us-section">
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="container">
                <div class="row">
                    <div class="section-spacing">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12">
                                    <div class="d-flex flex-column align-items-center justify-content-center">
                                        <h3>Contact us</h3>
                                        <div class="para-top-spacing"></div>
                                        <p class="text-center">
                                            Keep up to date with the latest news in projects, including
                                            <br>
                                            recent announcements and new research.
                                        </p>
                                        <div class="para-bottom-spacing"></div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6 mb-5">
                                    <div
                                        class="contact-us-img-wrapper h-100 d-flex align-items-center justify-content-center">
                                        <img class="img-fluid" src="{{ asset('assets/imgs/contact-us-form.png') }}"
                                             alt="startup about us img">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="contact-us-form-wrapper">
                                        <form class="contact-us-form col-12 col-md-10 offset-md-1" method="post"
                                              action="{{ route('contacts.store') }}" enctype="multipart/form-data">
                                            @csrf
                                            <div>
                                                <div class="form-group mb-4">
                                                    <input type="text"
                                                           class="form-control @error('first_name') is-invalid @enderror"
                                                           id="first-name"
                                                           name="first_name" value="{{ old('first_name') }}"
                                                           aria-describedby="first-name" placeholder="First name">
                                                    @error('first_name')
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group mb-4">
                                                <input type="text"
                                                       class="form-control @error('last_name') is-invalid @enderror"
                                                       id="last-name" name="last_name"
                                                       aria-describedby="last-name" placeholder="Last name"
                                                       value="{{ old('last_name') }}">
                                                @error('last_name')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-4">
                                                <input type="email"
                                                       class="form-control @error('email') is-invalid @enderror"
                                                       id="email" name="email"
                                                       aria-describedby="email" placeholder="Your email"
                                                       value="{{ old('email') }}">
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-4">
                                                <textarea class="form-control @error('message') is-invalid @enderror"
                                                          id="description" rows="6"
                                                          name="description">{{ old('description') }}</textarea>
                                                @error('description')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <button type="submit" class="btn btn-default contact-form-button">Submit
                                            </button>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container-fluid">
        <div class="row m-0">
            <div class="container">
                <div class="row">
                    <div class="footer-section-spacing">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-sm-3">
                                    <p class="footer-title">MIMO FINDS</p>
                                    <div class="footer-img-wrapper">
                                        <a href="{{ route('index') }}">
                                            <img class="img-fluid"
                                                 style="margin-left: 10px !important;"
                                                 src="{{ asset('assets/imgs/MimoFinds_Logo_Watersign.png') }}"
                                                 width="70" alt="startup about us img">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3 mt-4 mt-sm-0">
                                    <p class="footer-title">Links</p>
                                    <ul class="footer-list">
                                        <li><a href="#about-us-section">About us</a></li>
                                        <li><a href="#our-team-section">Our team</a></li>
                                        <li><a href="#contact-us-section">Contact us</a></li>
                                    </ul>
                                </div>
                                @guest
                                    <div class="col-12 col-sm-3 mt-4 mt-sm-0">
                                        <p class="footer-title">Register</p>
                                        <ul class="footer-list">
                                            <li><a href="javascript:void(0);" data-toggle="modal"
                                                   data-target="#startupsRegisterModal">Startup</a></li>
                                            <li><a href="javascript:void(0);" data-toggle="modal"
                                                   data-target="#investorRegisterModal">Investor</a></li>
                                        </ul>
                                    </div>
                                @endguest

                                <div class="col-12 col-sm-3 mt-4 mt-sm-0">
                                    <p class="footer-title">Social</p>
                                    <ul class="footer-list">
                                        <li>
                                            <a href="https://twitter.com/mimofinds?s=11" class="mr-1">
                                                    <span class="team-social-icons twitter-icon text-center">
                                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                                    </span>
                                            </a>
                                            <a href="https://www.instagram.com/mimofinds.ch/">
                                                    <span class="team-social-icons linkedin-icon text-center">
                                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                                    </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <hr class="footer-hr">

                                <p class="copyrights w-100 d-flex justify-content-between px-3 px-sm-0">

                                    <span> Mimo Finds</span>

                                    <span> All Rights Reserved &copy; <span id="current-year"></span></span>
                                </p>
                                <div class="row w-100">
                                    <div class="col-md-2">

                                    </div>
                                    <div class="col-md-8">
                                        <p class="privacy">
                                            <a href="javascript:void(0)" class="white-color" data-toggle="modal"
                                               data-target="#data-protection">Data Protection</a>
                                            <a href="javascript:void(0)" class="white-color" data-toggle="modal"
                                               data-target="#imprint"> | Imprint</a>
                                            <a href="javascript:void(0)" class="white-color" data-toggle="modal"
                                               data-target="#gtc"> | GTC</a>
                                        </p>

                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Login Boostrap Modals -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <form class="startups-form-group" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="startups-modal-header mb-4 position-relative">
                        <h5 class="modal-title startups-modal-title" id="startupsRegisterModalLabel">Login</h5>
                        <button type="button" class="close startups-modal-header-close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                               id="startupsInputEmail" name="email" aria-describedby="startupsInputEmail"
                               placeholder="Email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control  @error('password') is-invalid @enderror"
                               id="startupsInputPassword" name="password" placeholder="Password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-default btn-design-primary mr-2">Login</button>
                    <button type="button" class="btn btn-default btn-design-secondary" data-dismiss="modal">Cancel
                    </button>
                    <a class="btn btn-link" href="/forget-password">
                        Forgot Your Password?
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Startups Boostrap Modals -->
<div class="modal fade" id="startupsRegisterModal" tabindex="-1" role="dialog" aria-labelledby="startupsRegisterModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <form class="startups-form-group" method="POST" action="{{ route('register') }}">
                    @csrf
                    <input type="hidden" class="form-control" id="user_type" name="user_type"
                           aria-describedby="startupsInputUsername" value="startup">
                    <div class="startups-modal-header mb-4 position-relative">
                        <h5 class="modal-title startups-modal-title" id="startupsRegisterModalLabel">Startups
                            Registeration</h5>
                        <button type="button" class="close startups-modal-header-close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control @error('startupsInputUsername') is-invalid @enderror"
                               id="startupsInputUsername" name="name" aria-describedby="startupsInputUsername"
                               placeholder="Username">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
             <strong>{{ $message }}</strong>
                 </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control  @error('startupsInputEmail') is-invalid @enderror"
                               id="startupsInputEmail" name="email" aria-describedby="startupsInputEmail"
                               placeholder="Email">
                        @error('startupsInputEmail')
                        <span class="invalid-feedback" role="alert">
             <strong>{{ $message }}</strong>
                 </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="startupsInputNumber" name="phone_number"
                               aria-describedby="startupsInputNumber" placeholder="Telephone Number">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control @error('startupsInputPassword') is-invalid @enderror"
                               id="startupsInputPassword" name="password" placeholder="Password" required
                               autocomplete="new-password">
                        @error('startupsInputPassword')
                        <span class="invalid-feedback" role="alert">
             <strong>{{ $message }}</strong>
                 </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="startupsInputPassword"
                               name="password_confirmation" placeholder="Confirm Password" required
                               autocomplete="new-password">
                    </div>
                    <div class="form-group mt-4 mb-2">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="male">
                            <label class="form-check-label" for="inlineRadio1">Male</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="inlineRadio2" name="gender" value="female">
                            <label class="form-check-label" for="inlineRadio2">Female</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="inlineRadio3" name="gender" value="other">
                            <label class="form-check-label" for="inlineRadio3">Other</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="startupsInputName"
                               aria-describedby="startupsInputName" name="full_name" placeholder="Name (Sur & last)">
                    </div>
                    <input type="hidden" class="form-control" id="investor-add-form" name="description" value="no">
                    <div class="form-group mb-4">

                        <input type="checkbox" class="form-control" id="terms_and_condtions" name="terms_and_condtions"
                               style="width: 5% !important;display: inline-block !important;"
                               value="1" required> <span>Please Accept Terms & Conditions</span>
                    </div>
                    <button type="submit" class="btn btn-default btn-design-primary mr-2">Register</button>
                    <button type="button" class="btn btn-default btn-design-secondary" data-dismiss="modal">Cancel
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Investor Boostrap Modals -->
<div class="modal fade" id="investorRegisterModal" tabindex="-1" role="dialog" aria-labelledby="investorRegisterModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <form class="investor-form-group" method="POST" action="{{ route('register') }}">
                    @csrf
                    <input type="hidden" class="form-control" id="user_type" name="user_type"
                           aria-describedby="investorInputUsername" value="investor">
                    <div class="investor-modal-header mb-4 position-relative">
                        <h5 class="modal-title investor-modal-title" id="investorRegisterModalLabel">Investor
                            Registeration</h5>
                        <button type="button" class="close investor-modal-header-close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="investorInputUsername" name="name"
                               aria-describedby="investorInputUsername" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="investorInputEmail"
                               aria-describedby="investorInputEmail" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="phone_number" id="investorInputNumber"
                               aria-describedby="investorInputNumber" placeholder="Telephone Number">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="investorInputPassword" name="password"
                               placeholder="Password" required autocomplete="new-password">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="startupsInputPassword"
                               name="password_confirmation" placeholder="Confirm Password" required
                               autocomplete="new-password">
                    </div>
                    <div class="form-group mt-4 mb-2">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="male">
                            <label class="form-check-label" for="inlineRadio1">Male</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="female">
                            <label class="form-check-label" for="inlineRadio2">Female</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="gender" id="inlineRadio3" value="other">
                            <label class="form-check-label" for="inlineRadio3">Other</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="investorInputName"
                               aria-describedby="investorInputName" name="full_name" placeholder="Name (Sur & last)">
                    </div>
                    <div class="form-group mb-4">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="investor-add-form" name="description" rows="4"
                                  required></textarea>
                    </div>
                    <div class="form-group mb-4">
                        <input type="checkbox" class="form-control" id="terms_and_condtions" name="terms_and_condtions"
                               style="width: 5% !important;display: inline-block !important;"
                               value="1" required> <span>Please Accept Terms & Conditions</span>
                    </div>
                    <button type="submit" class="btn btn-default btn-design-primary mr-2">Register</button>
                    <button type="button" class="btn btn-default btn-design-secondary" data-dismiss="modal">Cancel
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>


{{--Data Protection--}}
<div class="modal fade" id="data-protection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Data Protection</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since the 1500s
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

{{--Imprint--}}
<div class="modal fade" id="imprint" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Imprint</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since the 1500s
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

{{--GTC--}}
<div class="modal fade" id="gtc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">General Terms & Conditions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since the 1500s
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="{{ asset('js/toastr.min.js') }}"></script>
<script>
    @if(Session::has('flash_message'))
    var type = "{{ Session::get('flash_status') }}";
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('flash_message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('flash_message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('flash_message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('flash_message') }}");
            break;
    }
    @endif

</script>
<!-- Owl Carousel Min Js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
        integrity="sha512-bPs7Ae6pVvhOSiIcyUClR7/q2OAsRiovw4vAkX+zJbw3ShAeeqezq50RIIcIURq7Oa20rW2n2q+fyXBNcU9lrw=="
        crossorigin="anonymous"></script>
<!-- Owl Carousel Slider Script -->
<script>
    $(document).ready(function () {
        $('.img-sliders-wrapper').owlCarousel({
            loop: true,
            dots: false,
            nav: true,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true,
            items: 2,
            responsive: {
                0: {
                    items: 1
                }
            }
        });
    });
</script>

<!-- Get Current Year -->
<script>
    $(document).ready(function () {
        var year = new Date().getFullYear();
        $('#current-year').text(year);
    })
</script>

</body>
</html>
