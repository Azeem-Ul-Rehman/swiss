@extends('pages.app')
@section('title','Payment')
@section('content')
    <section id="startups-information-section">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="section-spacing startup-section-spacing">
                    <div class="container">
                        <div class="row">
                            <div class="startups-information-wrapper">
                                <div
                                    class="investors-information-header d-flex align-items-center justify-content-between mb-3">
                                    <h4 class="">Pay for {{$price}} CHF/Month to Complete Your Registration
                                        Process. </h4>
                                </div>
                                <hr>
                                @if (Session::has('flash_message'))
                                    <div
                                        class="alert {{ Session::get('flash_status') == 'success' ? 'alert-success' : 'alert-danger' }} text-center">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                                        <p>{{ Session::get('flash_message') }}</p><br>
                                    </div>
                                @endif
                                <br>
                                <div class="col-12 p-0">
                                    <div class="row">


                                        <form class="col-12 col-sm-5 require-validation"
                                              action="{{ route('payment.post') }}" method="post"
                                              data-cc-on-file="false"
                                              data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                                              id="payment-form">
                                            @csrf
                                            <input type="hidden" name="amount" value="{{$price}}">
                                            <div
                                                class="form-group mb-4 @error('card_holder_name') is-invalid @enderror">

                                                <label class='control-label' for="card_holder_name">Card Holder
                                                    Name</label>
                                                <input type="text" class="form-control" id="card_holder_name"
                                                       name="card_holder_name" aria-describedby="card_holder_name"
                                                       value="{{ old('card_holder_name') }}"
                                                       placeholder="Card Holder Name">
                                                @error('card_holder_name')
                                                <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-4 @error('card-number') is-invalid @enderror">

                                                <label class='control-label' for="card-number">Card Number</label>
                                                <input type="text" class="form-control card-number" id="card-number"
                                                       name="card-number" aria-describedby="card-number"
                                                       value="{{ old('card-number') }}"
                                                       placeholder="xxxx xxxx xxxx xxxx">
                                                @error('card-number')
                                                <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class='form-row row'>
                                                <div
                                                    class='col-xs-12 col-md-4 form-group cvc required @error('card-number') is-invalid @enderror'>
                                                    <label class='control-label' for="card-cvc">CVC</label>
                                                    <input autocomplete='off' class='form-control card-cvc'
                                                           placeholder='ex. 311'
                                                           size='4'
                                                           id="card-cvc"
                                                           name="card-cvc"
                                                           aria-describedby="card-cvc"
                                                           value="{{ old('card-cvc') }}"
                                                           type='text'>
                                                    @error('card-cvc')
                                                    <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div
                                                    class='col-xs-12 col-md-4 form-group expiration required @error('card-expiry-year') is-invalid @enderror'>
                                                    <label class='control-label' for="card-expiry-month">Exp
                                                        Month</label>
                                                    <input class='form-control card-expiry-month' placeholder='MM'
                                                           id="card-expiry-month"
                                                           name="card-expiry-month"
                                                           aria-describedby="card-expiry-month"
                                                           value="{{ old('card-expiry-month') }}"
                                                           size='2' type='text'>
                                                    @error('card-expiry-month')
                                                    <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                                <div
                                                    class='col-xs-12 col-md-4 form-group expiration required @error('card-expiry-year') is-invalid @enderror'>
                                                    <label class='control-label' for="card-expiry-year">Exp Year</label>
                                                    <input class='form-control card-expiry-year' placeholder='YYYY'
                                                           size='4'
                                                           id="card-expiry-year"
                                                           name="card-expiry-year"
                                                           aria-describedby="card-expiry-year"
                                                           value="{{ old('card-expiry-year') }}"
                                                           type='text'>
                                                    @error('card-expiry-year')
                                                    <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class='form-row row'>
                                                <div class='col-md-12 error form-group hide' style="display: none">
                                                    <div class='alert-danger alert'>Please correct the errors and try
                                                        again.
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-row row">
                                                <div class="col-xs-12">
                                                    <button class="btn btn-primary btn-lg btn-block" type="submit">{{$price}}
                                                        CHF Pay
                                                        Now
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('js')
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        $(function () {
            var $form = $(".require-validation");
            $('form.require-validation').bind('submit', function (e) {
                var $form = $(".require-validation"),
                    inputSelector = ['input[type=email]', 'input[type=password]', 'input[type=text]', 'input[type=file]', 'textarea'].join(', '),
                    $inputs = $form.find('.required').find(inputSelector),
                    $errorMessage = $form.find('div.error'),
                    valid = true;
                $errorMessage.addClass('hide');
                $('.has-error').removeClass('has-error');
                $inputs.each(function (i, el) {
                    var $input = $(el);
                    if ($input.val() === '') {
                        $input.parent().addClass('has-error');
                        $errorMessage.removeClass('hide');
                        e.preventDefault();
                    }
                });
                if (!$form.data('cc-on-file')) {
                    e.preventDefault();
                    Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                    Stripe.createToken({
                        number: $('.card-number').val(),
                        cvc: $('.card-cvc').val(),
                        exp_month: $('.card-expiry-month').val(),
                        exp_year: $('.card-expiry-year').val()
                    }, stripeResponseHandler);
                }
            });

            function stripeResponseHandler(status, response) {
                if (response.error) {
                    $('.error')
                        .css('display', '')
                        .find('.alert')
                        .text(response.error.message);
                } else {
                    /* token contains id, last4, and card type */
                    var token = response['id'];
                    $form.find('input[type=text]').empty();
                    $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                    $form.get(0).submit();
                }
            }
        });
    </script>
@endpush
