@extends('pages.app')
@section('title','Investor Account')
@section('content')
    <section id="startups-information-section">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="section-spacing investor-section-spacing">
                    <div class="container">
                        <div class="row">
                            <div class="startups-information-wrapper">
                                <div
                                    class="investors-information-header d-flex align-items-center justify-content-between mb-3">
                                    <span class="investors-information-header-title">Investor Details</span>
                                </div>
                                <hr>
                                <div class="col-12 startups-imgs-idea p-0">
                                    <div class="row">

                                        <form method="POST" action="{{route('investor.update')}}"
                                              class="col-12 col-md-5 col-lg-5" enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group mb-4">
                                                <label for="name">Name</label>
                                                <input type="text" class="form-control" id="name" name="name"
                                                       value="{{old('name',$user->name)}}" aria-describedby="name"
                                                       placeholder="Name" required>
                                            </div>
                                            <div class="form-group mb-4">
                                                <label for="full_name">Full Name</label>
                                                <input type="text" class="form-control" id="full_name" name="full_name"
                                                       value="{{old('full_name',$user->full_name)}}"
                                                       aria-describedby="full_name"
                                                       placeholder="Full Name" required>
                                            </div>
                                            <div class="form-group mb-4">
                                                <label for="email">Email</label>
                                                <input type="text" class="form-control" id="email" name="email"
                                                       value="{{old('email',$user->email)}}" aria-describedby="email"
                                                       placeholder="Email" required>
                                            </div>
                                            <div class="form-group mb-4">
                                                <label for="phone_number">Phone Number</label>
                                                <input type="text" class="form-control" id="phone_number"
                                                       name="phone_number"
                                                       value="{{old('phone_number',$user->phone_number)}}"
                                                       aria-describedby="phone_number"
                                                       placeholder="Phone Number" required>
                                            </div>
                                            <div class="form-group mb-4">
                                                <label for="gender">Gender</label>
                                                <input type="text" class="form-control" id="gender" name="gender"
                                                       value="{{old('gender',$user->gender)}}" aria-describedby="gender"
                                                       placeholder="Gender" required>
                                            </div>

                                            <div class="form-group mb-4">
                                                <label for="description">Description</label>
                                                <textarea class="form-control" id="description" name="description"
                                                          rows="4"
                                                          required>{{ old('description',$user->description) }}</textarea>
                                            </div>
                                            <div class="my-lg-0 mr-3 position-relative">
                                                <button class="btn btn-default my-2 my-sm-0 design-button">
                                                    <span class="design-button-span">Submit</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
