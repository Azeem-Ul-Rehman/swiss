<style>
    .badge {
        background: red !important;
        display: inline-block;
        padding: 0.25em .4em !important;
        font-size: 78% !important;
        font-weight: 700;
        line-height: 1;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: 50% !important;
        color: #fff !important;
    }

</style>
<header class="m-0">
    <div class="container">
        <div class="row">
            <div class="col-12 p-0">
                <nav class="navbar navbar-light bg-light navbar-expand-md">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                        <a class="navbar-brand" href="{{ route('index') }}"><img
                                src="{{ asset('assets/imgs/main-logo-3.png') }}" width="170"
                                alt="company logo"/></a>
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                            <li class="nav-item active">
                                <a class="nav-link px-md-0 px-lg-2" href="{{ route('index') }}">Home <span
                                        class="sr-only">(current)</span></a>
                            </li>
                            @auth
                                <li class="nav-item">
                                    <a class="nav-link px-md-0 px-lg-2" href="{{ route('discover') }}">Discover</a>
                                </li>
                                @if(\Illuminate\Support\Facades\Auth::check())
                                    @if(Auth::user()->user_type == 'investor')
                                        @php
                                            $investorCount = \App\Message::where('to',Auth::user()->id)->where('is_read',false)->count();
                                        @endphp
                                        <li class="nav-item">
                                            <a class="nav-link px-md-0 px-lg-2" href="{{ route('investor.index') }}">My
                                                account</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link px-md-0 px-lg-2"
                                               href="{{ route('investor-message-all.index') }}">Inbox <span class="badge">{{$investorCount}}</span></a>
                                        </li>
                                    @endif
                                    @if(Auth::user()->user_type == 'startup')
                                            @php
                                                $startupCount = \App\Message::where('to',Auth::user()->id)->where('is_read',false)->count();
                                            @endphp
                                        <li class="nav-item">
                                            <a class="nav-link px-md-0 px-lg-2"
                                               href="{{ route('startup-get.index') }}">Inbox <span class="badge">{{ $startupCount }}</span></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link px-md-0 px-lg-2" href="{{ route('startup.index') }}">My
                                                account</a>
                                        </li>
                                    @endif
                                @endif
                            @endauth
                        </ul>
                        @auth
                            <div class="form-inline my-2 my-lg-0 mr-3 position-relative">
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <button class="btn btn-default my-2 my-sm-0 design-button">
                                        <span class="design-button-span">Logout</span>

                                    </button>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        @endauth
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
