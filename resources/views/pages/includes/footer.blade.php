<footer>
    <div class="container-fluid">
        <div class="row m-0">
            <div class="container">
                <div class="row">
                    <div class="footer-section-spacing">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <p class="footer-title">MIMO FINDS</p>
                                    <div class="footer-img-wrapper">
                                        <a href="{{ route('index') }}">
                                            <img class="img-fluid"
                                                 style="margin-left: 10px !important;"
                                                 src="{{ asset('assets/imgs/MimoFinds_Logo_Watersign.png') }}"
                                                 width="70" alt="startup about us img">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-4 mt-4 mt-sm-0">
                                    <p class="footer-title">Links</p>
                                    <ul class="footer-list">
                                        <li><a href="{{ route('index') }}">Home</a></li>
                                        <li><a href="{{ route('discover') }}">Discover</a></li>
                                        @if(\Illuminate\Support\Facades\Auth::check())
                                            @if(Auth::user()->user_type == 'investor')
                                                <li><a href="{{ route('investor.index') }}">My account</a></li>
                                            @endif
                                            @if(Auth::user()->user_type == 'startup')
                                                <li><a href="{{ route('startup.index') }}">My account</a></li>
                                            @endif
                                        @endif
                                    </ul>
                                </div>
                                {{--                                <div class="col-12 col-sm-3 mt-4 mt-sm-0">--}}
                                {{--                                    <p class="footer-title">Register</p>--}}
                                {{--                                    <ul class="footer-list">--}}
                                {{--                                        <li><a href="javascript:void(0);" data-toggle="modal"--}}
                                {{--                                               data-target="#startupsRegisterModal">Startup</a></li>--}}
                                {{--                                        <li><a href="javascript:void(0);" data-toggle="modal"--}}
                                {{--                                               data-target="#investorRegisterModal">Investor</a></li>--}}
                                {{--                                    </ul>--}}
                                {{--                                </div>--}}

                                <div class="col-12 col-sm-4 mt-4 mt-sm-0">
                                    <p class="footer-title">Social</p>
                                    <ul class="footer-list">
                                        <li>
                                            <a href="https://twitter.com/mimofinds?s=11" class="mr-1">
                                                    <span class="team-social-icons twitter-icon text-center">
                                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                                    </span>
                                            </a>
                                            <a href="https://www.instagram.com/mimofinds.ch/">
                                                    <span class="team-social-icons linkedin-icon text-center">
                                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                                    </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <hr class="footer-hr">
                                <p class="copyrights w-100 d-flex justify-content-between px-3 px-sm-0">
                                    <span> MIMO FINDS </span>
                                    <span> All Rights Reserved &copy; <span id="current-year"></span></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
