<div class="message-right-wrapper">
    <div class="message-right-body messages">
        <div class="people-pic-wrapper-header">
            <div class="people-pic-wrapper-right">
                <span class="bg-success person-active-status-right"></span>
                <span class="person-img-right">
                    <img src="{{ asset('assets/imgs/email.png') }}">
                </span>
            </div>
            <div class="people-message-text-wrapper-right">
                <h6>{{$currentUser->name}}</h6>
                {{--                <i class="fa fa-trash trash-icon" aria-hidden="true"></i>--}}
                <p class="text-muted">Online</p>
            </div>
        </div>
        <div class="people-pic-wrapper-body position-relative">

            @if(!empty($messages) && count($messages) > 0)
                @foreach($messages as $message)
                    <div
                        class="message clearfix {{ (($message->from ==  Auth::id()) && ($message->type == 'customer')) ? 'right-chat-messages' :'left-chat-messages'  }}"
                        id="message_sent">
                        <div
                            class="{{ (($message->from ==  Auth::id()) && ($message->type == 'customer')) ? 'actual-message-right' : 'actual-message-left' }}">
                                <span id="message_sent">
                                   {{ $message->message }}
                                    <br>
                                    <span
                                        class="{{ (($message->from ==  Auth::id()) && ($message->type == 'customer')) ? 'text-muted' : 'text-muted-right' }}  current-time">{{ date('d M y, h:i a', strtotime($message->created_at)) }}</span>
                                </span>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="chosen-files-wrapper">
        </div>
        <div class="people-pic-wrapper-footer position-relative">
            <div class="to-be-sent-messages">
                <div class="form-inline">
                        <span class="clip-icon-wrapper">
{{--                            <input type="file" id="attached-files" name="attached-files"--}}
                            {{--                                   hidden="hidden">--}}
                            <i class="fa fa-paperclip clip-icon" aria-hidden="true"
                               style="font-size: 0px !important;"></i>
                        </span>
                    <div class="form-group chat-send-input">
                        <input type="text" name="message" id="text" class="form-control" placeholder="Message...">
                    </div>

                    <button type="button" class="btn btn-default btn-send" onclick="sendClick(event)">Send</button>

                </div>
            </div>
        </div>
    </div>
</div>


