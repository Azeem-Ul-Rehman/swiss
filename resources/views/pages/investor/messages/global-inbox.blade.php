@extends('pages.app')
@section('title','Inbox')
@push('css')
    <style>
        /* width */
        ::-webkit-scrollbar {
            width: 7px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #2b3a5e;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #2b3a5e;
        }

        /*.pending {*/
        /*    position: relative;*/
        /*    left: -293px;*/
        /*    top: -50px;*/
        /*    background: #51106b;*/
        /*    margin: 0;*/
        /*    border-radius: 50%;*/
        /*    width: 18px;*/
        /*    height: 18px;*/
        /*    line-height: 18px;*/
        /*    padding-left: 5px;*/
        /*    color: #ffffff;*/
        /*    font-size: 10px;*/
        /*    font-weight: 500;*/
        /*    border: 1px solid #e3e3e3;*/
        /*}*/

        .pending {
            position: absolute;
            left: -6px;
            /* top: -50px; */
            background: #b600ff;
            /* margin: 0; */
            border-radius: 50%;
            width: 18px;
            height: 18px;
            line-height: 18px;
            padding-left: 5px;
            color: #ffffff;
            font-size: 12px;
        }

        .activeMessage {
            color: #fff;
            background: #2b3a5e;
        }

        .activeMessage p {
            color: #fff !important;
        }


        /*    Media Queries*/

    </style>
@endpush
@section('content')

    <section id="startups-investors-messages">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-12 p-0">
                            <div class="row message-wrapper">
                                <div class="col-12 col-sm-4 p-0">
                                    <div class="message-left-wrapper">
                                        <div class="message-left-body">
                                            {{--                                            <div class="search-wrapper">--}}
                                            {{--                                                <form class="form-inline startups-investors-form">--}}
                                            {{--                                                    <input type="text" class="form-control-plaintext"--}}
                                            {{--                                                           id="messages_search" name="messages_search"--}}
                                            {{--                                                           placeholder="Search username">--}}
                                            {{--                                                    <span class="startups-investors-search-icon">--}}
                                            {{--                                                        <i class="fa fa-search search-icon" aria-hidden="true"></i>--}}
                                            {{--                                                    </span>--}}
                                            {{--                                                </form>--}}
                                            {{--                                            </div>--}}
                                            {{--                                            <div class="message-tabs-wrapper">--}}
                                            {{--                                                <ul class="list-inline d-flex align-items-center justify-content-center">--}}
                                            {{--                                                    <li class="list-inline-item messages-active-item">All</li>--}}
                                            {{--                                                    <li class="list-inline-item">Read</li>--}}
                                            {{--                                                    <li class="list-inline-item">Unread</li>--}}
                                            {{--                                                </ul>--}}
                                            {{--                                            </div>--}}
                                            <div class="people-message-wrapper">

                                                @if(!empty($messages) && count($messages) > 0)
                                                    @foreach($messages as $message)
                                                        @if(!is_null($message->startup))
                                                            <div class="people-message user"

                                                                 data-discover-id='{{$message->discover_id}}'
                                                                 data-user-id='{{$message->user->id}}'>
                                                                <div class="people-pic-wrapper"  id="{{ $message->discover_id }}">
                                                                    <span class="bg-success person-active-status"></span>

                                                                    <span class="person-img">
                                                                    <img
                                                                        src="{{ asset('assets/imgs/person.png') }}"/>

                                                                    </span>

                                                                    @if($message->unread)
                                                                        <span
                                                                            class="pending">{{ $message->unread }}</span>
                                                                    @endif
                                                                </div>
                                                                <div class="people-message-text-wrapper">
                                                                    <h6>{{ $message->user->name }}
                                                                        ({{$message->startup->name_startup}})</h6>
                                                                    {{--                                                                                <i class="fa fa-trash trash-icon"--}}
                                                                    {{--                                                                                   aria-hidden="true"></i>--}}
                                                                    <p>{{ $message->user->email }}
                                                                    </p>
                                                                </div>

                                                            </div>
                                                        @endif

                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-8 p-0" id="messages">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection
@push('js')

    <script>
        var receiver_id = '';
        var discover_id = '';
        var my_id = "{{ Auth::id() }}";

        function sendClick(e) {
            var message = $('#text').val();

            // check if enter key is pressed and message is not null also receiver is selected
            if (message != '' && receiver_id != '') {
                $(this).val(''); // while pressed enter text box will be empty

                var token = $('meta[name="csrf-token"]').attr('content');
                var datastr = "_token=" + token + "&discover_id=" + discover_id + "&receiver_id=" + receiver_id + "&message=" + message;
                $.ajax({
                    type: "post",
                    url: "{{ \Illuminate\Support\Facades\URL::route('store-messages') }}", // need to create this post route
                    data: datastr,
                    cache: false,
                    success: function (data) {

                    },
                    error: function (jqXHR, status, err) {
                    },
                    complete: function () {
                        scrollToBottomFunc();
                    }
                })
            } else {
                alert('Please type some message to send');
            }
        }

        $(document).ready(function () {
            // ajax setup form csrf token
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // Enable pusher logging - don't include this in production
            Pusher.logToConsole = true;

            var pusher = new Pusher('0ea14e6e7c66a09c4a7d', {
                cluster: 'ap2',
                forceTLS: false
            });


            var channel = pusher.subscribe('my-channel');
            channel.bind('my-event', function (data) {

                // alert(JSON.stringify(data));
                if (my_id == data.from && data.discover_id == discover_id) {
                    $('#' + discover_id).click();
                } else if (my_id == data.to && data.discover_id == discover_id) {
                    if (receiver_id == data.from) {
                        // if receiver is selected, reload the selected user ...
                        $('#' + discover_id).click();
                    } else {

                        // if receiver is not seleted, add notification for that user
                        var pending = parseInt($('#' + discover_id).find('.pending').html());

                        if (pending) {
                            $('#' + discover_id).find('.pending').html(pending + 1);
                        } else {
                            $('#' + discover_id).append('<span class="pending">1</span>');
                        }
                    }
                } else {
                    // if receiver is not seleted, add notification for that user
                    var pending = parseInt($('#' + data.discover_id).find('.pending').html());

                    if (pending) {
                        $('#' + data.discover_id).find('.pending').html(pending + 1);
                    } else {
                        $('#' + data.discover_id).append('<span class="pending">1</span>');

                    }
                }
            });

            $('.user').click(function () {
                $('.user').removeClass('activeMessage');
                discover_id = $(this).data('discover-id');
                $(this).addClass('activeMessage');
                $(this).find('.pending').remove();

                receiver_id = $(this).data('user-id');

                $.ajax({
                    type: "get",
                    url: "{{ route('get-messages') }}", // need to create this route
                    data: {'id': receiver_id, 'discover_id': discover_id},
                    cache: false,
                    success: function (data) {
                        $('#messages').html(data);
                        scrollToBottomFunc();
                    }
                });
            });

            $(document).on('keyup', '.chat-send-input input', function (e) {
                var message = $(this).val();

                // check if enter key is pressed and message is not null also receiver is selected
                if (e.keyCode == 13 && message != '' && receiver_id != '') {
                    $(this).val(''); // while pressed enter text box will be empty
                    var token = $('meta[name="csrf-token"]').attr('content');
                    // var datastr = "receiver_id=" + receiver_id + "&message=" + message;
                    var datastr = "_token=" + token + "&discover_id=" + discover_id + "&receiver_id=" + receiver_id + "&message=" + message;
                    $.ajax({
                        type: "post",
                        url: "{{ \Illuminate\Support\Facades\URL::route('store-messages') }}", // need to create this post route
                        data: datastr,
                        cache: false,
                        success: function (data) {

                        },
                        error: function (jqXHR, status, err) {
                        },
                        complete: function () {
                            scrollToBottomFunc();
                        }
                    })
                }
            });
        });

        // make a function to scroll down auto
        function scrollToBottomFunc() {
            $('.message-right-wrapper').animate({
                scrollTop: $('.message-right-wrapper').get(0).scrollHeight
            }, 50);
        }
    </script>
    <!-- Attach files -->
    <script>
        $(document).ready(function () {
            let fixedChatBodyHeight = chatBodyHeight = chatFilesHeight = 0;
            fixedChatBodyHeight = $('.people-pic-wrapper-body').height();

            $('.clip-icon').click(function () {
                $('#attached-files').click();
            })

            $('#attached-files').change(function () {
                var attachedFile = $('#attached-files').val();
                if (attachedFile) {
                    $('.chosen-files-wrapper').show();
                    $('.chosen-files-text').text(attachedFile);
                    $('.chosen-files-wrapper').prepend(
                        '<div class="chosen-files-items position-relative d-flex align-items-center jusitfy-content-center">'
                        + '<div class="chat-link-file-anchor">'
                        + '<a href="' + attachedFile + '" download>' + attachedFile.match(/[\/\\]([\w\d\s\.\-\(\)]+)$/)[1] + '</a>'
                        + '</div>'
                        + '<div class="chat-link-file-close text-right">'
                        + '<span class="bg-danger text-white chat-files-close">&times;</span>'
                        + '</div>'
                        + '</div>'
                    );

                    chatFilesHeight = $('.chosen-files-wrapper').height();
                    chatBodyHeight = fixedChatBodyHeight - chatFilesHeight;
                    $('.people-pic-wrapper-body').css({'height': chatBodyHeight});
                }
            });

            // Remove Attach files
            let chatBodyHeightAfter = chatFilesHeightAfter = 0;

            $(document).on('click', '.chat-files-close', function () {
                chatBodyHeight = $('.people-pic-wrapper-body').height();
                chatFilesHeight = $('.chosen-files-wrapper').height() + 20; // 20 is top + bottom padding
                alert(chatFilesHeight);

                $(this).closest('.chosen-files-items').remove();

                chatFilesHeightAfter = $('.chosen-files-wrapper').height();
                alert(chatFilesHeightAfter);

                chatFilesHeightAfter = chatFilesHeight - chatFilesHeightAfter;
                chatBodyHeight = chatBodyHeight + chatFilesHeightAfter

                $('.people-pic-wrapper-body').css({'height': chatBodyHeight});

                if ($('.chosen-files-wrapper').children().length < 1) {
                    $('.chosen-files-wrapper').css('display', 'none');
                    $('.people-pic-wrapper-body').css({'height': chatBodyHeight + 20});
                }
            })
        })
    </script>
@endpush
