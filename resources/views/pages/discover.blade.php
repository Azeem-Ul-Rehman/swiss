@extends('pages.app')
@section('title','Discover')


@section('content')
    <section id="startups-information-section">
        <div class="container-fluid">
            <div class="row">
                <div class="section-spacing investor-section-spacing py-5">
                    <div class="container">
                        <div class="row">
                            <h1 class="page-title px-3 px-md-0"> Discover Startups</h1>
                            <hr class="page-title-hr">
                            <nav class="w-100 navbar px-md-0 mb-3">
                                <form class="form-inline mb-3 mb-md-0">
                                    <input class="form-control ideas-list-search" type="search" placeholder="Search..."
                                           aria-label="Search">
                                </form>
                                <form class="form-inline mb-3 mb-md-0">
                                    <span class="mr-3 text-muted">Price in dollars:</span>
                                    <input type="range" min="0" max="1000000" value="0" class="range-slider"
                                           step="5000">
                                    <span class="ml-4 range-value text-muted">0</span>
                                </form>
                            </nav>

                            <div class="startups-information-wrapper">
                                <div class="col-12 startups-ideas-list">
                                    <div class="row">
                                        <div class="startup-pitches-list">
                                            <!-- Startup pitches list comes here -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('js')
    <!-- Search From List -->
    <script>
        var dataJson = {!! json_encode($discover,true) !!};
        var userType = "{{ Auth::user()->user_type }}";
        var pitches = '';
        $(document).ready(function () {
            let searchWord = rangeSlider = pitches = rangeVal = "";

            function listDataOnLoad() {
                pitches = '';
                if (dataJson.length > 0) {
                    dataJson.forEach(function (item) {
                        htmlAppend(item);
                    });
                    $('.startup-pitches-list').html(pitches);
                }
            }

            listDataOnLoad();

            // if the search result is false
            function dataNotFound() {
                pitches = '<div class="startup-pitches">'
                    + '<div>'
                    + '<span>'
                    + '<h5 class="startup-heading">Try again. No result found!</h5>'
                    + '</span>'
                    + '</div>'
                    + '</div>';

                $('.startup-pitches-list').append(pitches);
            }

            function listData() {
                // convert string to integer
                rangeSlider = parseInt($('.range-slider').val());
                searchWord = $('.ideas-list-search').val();
                $('.startup-pitches-list').empty();

                // if search field is empty
                if ((searchWord == "" || searchWord == null || searchWord == undefined) && rangeSlider == 0) {
                    listDataOnLoad();
                } else if ((searchWord == "" || searchWord == null || searchWord == undefined) && rangeSlider > 0) {
                    pitches = '';
                    dataJson.forEach(function (item) {
                        // if the search result is true
                        if (rangeSlider > 0 && parseInt(item.price_startup) <= rangeSlider) {
                            htmlAppend(item);
                        }
                    });
                    $('.startup-pitches-list').html(pitches);
                } else if (searchWord.length > 0 && rangeSlider == 0) {
                    pitches = '';
                    dataJson.forEach(function (item) {
                        // if the search result is true
                        if ((item.name_startup.search(searchWord) > -1 || item.branche.search(searchWord) > -1)) {
                            htmlAppend(item);
                        }
                    });
                    $('.startup-pitches-list').html(pitches);
                } else if (searchWord.length > 0 && rangeSlider > 0) {
                    pitches = '';
                    dataJson.forEach(function (item) {
                        // if the search result is true
                        if ((item.name_startup.search(searchWord) > -1 || item.branche.search(searchWord) > -1) && (rangeSlider > 0 && parseInt(item.price_startup) <= rangeSlider)) {
                            htmlAppend(item);
                        }
                    });
                    $('.startup-pitches-list').html(pitches);
                }

                // if the search result is false
                if ($('.startup-pitches-list').html().length < 1) {
                    dataNotFound();
                }
            }

            // Price Range Slider
            $('.range-slider').change(function () {

                $('.range-value').removeClass('text-muted');
                rangeVal = parseInt($('.range-slider').val());
                $('.range-value').text(rangeVal.toLocaleString("de-CH"));
                listData();
            })

            $('.ideas-list-search').keyup(function () {
                listData();
            })

            function htmlAppend(item) {

                var pict = JSON.parse(item.pictures);
                var image = '';
                if (item.pictures == null) {
                    image = '/assets/imgs/email.png';
                } else {
                    image = 'images/' + pict[0] + '';
                }

                var price = item.price_startup;
                pitches += '<div class="startup-pitches" >';
                pitches += '<div class="startup-pitches-left">';
                pitches += '<img class="img-fluid" src="' + image + '" width="100" alt="startup thumbnail"  style="width: 100% !important;"/>';
                pitches += '</div>';
                pitches += '<div class="startup-pitches-right">';
                pitches += '<a href="/startup/detail/' + item.id + '" id="startupId' + item.id + '">';
                pitches += '<div>';
                pitches += '<span>';
                pitches += '<h5 class="startup-heading">' + item.name_startup + '</h5>';
                pitches += '<p class="startup-category text-muted">' + item.branche + '</p>';
                pitches += '<h5 class="text-success mt-4 mb-3"><span class="required-amount-txt">Required Amount</span><br>$' + parseInt(price).toLocaleString("de-CH");
                +'</h5>';
                pitches += '</span>';

                if (userType != 'investor') {
                    // pitches += '<span class="investor-chat-wrapper d-flex align-items-center justify-content-center">';
                    // pitches += '<a href="/startup-message-all/' + item.id + '"><i class="fa fa-envelope envelope-icon" aria-hidden="true"></i></a>'
                    // pitches += '</span>';

                    pitches += '<a class="large-screen-pitches-icons large-screen-edit-btn" href="/startup/edit/' + item.id + '">';
                    pitches += '<span class="large-screen-edit-btn startups-pitches-icon">';
                    pitches += '<i class="fa fa-pencil" aria-hidden="true"></i>';
                    pitches += '</span>';
                    pitches += '</a>';
                    pitches += '<a class="large-screen-pitches-icons large-screen-edit-btn" href="/startup/delete/' + item.id + '">';
                    pitches += '<span class="large-screen-edit-btn startups-pitches-icon">';
                    pitches += '<i class="fa fa-trash" aria-hidden="true"></i>';
                    pitches += '</span>';
                    pitches += '</a>';
                } else {
                    pitches += '<span class="investor-chat-wrapper d-flex align-items-center justify-content-center">';
                    pitches += '<a href="/investor-message-inbox/' + item.id + '"><i class="investor-chat-wrapper-msg-icon fa fa-comments envelope-icon" aria-hidden="true"></i></a>'
                    pitches += '</span>';

                }
                pitches += '</div>';
                pitches += '<p class="startup-description">' + item.description_startup + '</p>';
                pitches += '</a>';
                pitches += '</div>';
                if (userType != 'investor') {
                    pitches += '<div class="mobile-screen-pitches-icons-wrapper">';
                    pitches += '<a class="mobile-screen-pitches-icons" href="/startup/edit/' + item.id + '">';
                    pitches += '<span class="mobile-screen-edit-btn startups-pitches-icon">';
                    pitches += '<i class="fa fa-pencil" aria-hidden="true"></i>';
                    pitches += '</span>';
                    pitches += '</a>';
                    pitches += '<a class="mobile-screen-pitches-icons" href="/startup/delete/' + item.id + '">';
                    pitches += '<span class="mobile-screen-edit-btn startups-pitches-icon">';
                    pitches += '<i class="fa fa-trash" aria-hidden="true"></i>';
                    pitches += '</span>';
                    pitches += '</a>';
                    pitches += '</div>';
                }
                pitches += '</div>';

            }
        });
    </script>

    <!-- Chat messenger show and hide -->
    <script>
        $(document).ready(function () {
            $('.investor-chat-wrapper, .investor-chat-wrapper .envlope-icon').click(function (e) {
                e.preventDefault();
                $('.investor-to-startups-conversation').show();
            });

            $('.header-close').click(function () {
                $('.investor-to-startups-conversation').hide();
            });

        })

        function confirmDelete(id) {
            var r = confirm("Are you sure you want to perform this action");
            if (r === true) {
                document.getElementById("deleteStartup" + id).submit();
            } else {
                return false;
            }
        }
    </script>

@endpush
