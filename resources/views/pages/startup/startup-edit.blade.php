@extends('pages.app')
@section('title','Startup Edit')
@push('css')
    <link rel="stylesheet" href="{{ asset('assets/dist/image-uploader.min.css') }}">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link
        href="https://fonts.googleapis.com/css?family=Lato:300,700|Montserrat:300,400,500,600,700|Source+Code+Pro&display=swap"
        rel="stylesheet">

    <style>
        .delete-photos, .delete-videos {
            cursor: pointer;
        }
    </style>
@endpush
@section('content')
    <section id="startups-information-section">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="section-spacing startup-section-spacing">
                    <div class="container">
                        <div class="row">
                            <div class="startups-information-wrapper">
                                <div
                                    class="investors-information-header d-flex align-items-center justify-content-between mb-3">
                                    <span class="investors-information-header-title">Edit Startup Details</span>
                                </div>
                                <hr>
                                <div class="col-12 startups-imgs-idea p-0">
                                    <div class="row">
                                        <form class="col-12 col-sm-5" action="{{ route('startup.update') }}"
                                              method="post" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$startup->id}}">
                                            <div class="form-group mb-4 @error('name_startup') is-invalid @enderror">
                                                <label for="name_startup">Startup Name</label>
                                                <input type="text" class="form-control" id="name_startup"
                                                       name="name_startup" aria-describedby="name_startup"
                                                       value="{{ old('name_startup',$startup->name_startup) }}"
                                                       placeholder="Startup Name" required>
                                                @error('name_startup')
                                                <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-4 @error('branche') is-invalid @enderror">
                                                <label for="branche">Branch</label>
                                                <input type="text" class="form-control" id="branche" name="branche"
                                                       value="{{ old('branche',$startup->branche) }}"
                                                       aria-describedby="branche" placeholder="Branche" required>
                                                @error('branche')
                                                <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-4 @error('price_startup') is-invalid @enderror">
                                                <label for="price_startup">Price</label>
                                                <input type="text" class="form-control" id="price_startup"
                                                       value="{{ old('price_startup',$startup->price_startup) }}"
                                                       name="price_startup" aria-describedby="price_startup"
                                                       placeholder="Price" required>
                                                @error('price_startup')
                                                <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div
                                                class="form-group mb-4 @error('description_startup') is-invalid @enderror">
                                                <label for="description_startup">Description</label>
                                                <textarea class="form-control" id="description_startup"
                                                          name="description_startup"
                                                          aria-describedby="description_startup" rows="4"
                                                          required>{{ old('description_startup',$startup->description_startup) }}</textarea>
                                                @error('description_startup')
                                                <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-4 @error('images.*') is-invalid @enderror">
                                                <label for="images">Pictures</label>
                                                <div class="input-images-1" style="padding-top: .5rem;"></div>

                                                <div class="row">
                                                    @if(!empty($startup->galleries) && count($startup->galleries) > 0)
                                                        @foreach($startup->galleries as $key=>$img)
                                                            <div
                                                                class="col-4 p-0 p-md-2 col-md-3 mb-4 mb-md-0 idea-imgs-wrapper delete-photos"
                                                                data-remove-id="{{$img->id}}">
                                                                <span class="fa fa-trash"
                                                                      style="position: absolute; margin-top: 3px;padding-left: 72px;"></span>
                                                                <div
                                                                    class="idea-imgs d-flex align-items-center justify-content-center">
                                                                    <img src="{{ asset('images/'.$img->image.'') }}"
                                                                         class="rounded"
                                                                         alt="{{$img}}">
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                </div>
                                                {{--                                                <input type="file" class="form-control-file" id="pictures"--}}
                                                {{--                                                       name="pictures[]" multiple--}}
                                                {{--                                                       required>--}}
                                                @error('images.*')
                                                <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            {{--                                            <div class="form-group mb-4">--}}
                                            {{--                                                <label for="pictures">Pictures</label>--}}
                                            {{--                                                <input type="file" class="form-control-file" id="pictures"--}}
                                            {{--                                                       name="pictures[]" multiple--}}
                                            {{--                                                       required>--}}
                                            {{--                                            </div>--}}

                                            <div class="form-group mb-5">
                                                <label
                                                    for="imgs">Videos</label>
                                                <input type="file"
                                                       class="form-control-file"
                                                       id="videos"
                                                       name="videos[]"
                                                       multiple
                                                       required>
                                                @error('videos.*')
                                                <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                                <div class="row" style="margin-top: 24px;">
                                                    @if(!empty($startup->startupVideos) && count($startup->startupVideos) > 0)
                                                        @foreach($startup->startupVideos as $key=>$video)
                                                            <div
                                                                class="col-6 p-0 p-md-2 col-md-6 mb-4 mb-md-0 idea-imgs-wrapper delete-videos"
                                                                data-remove-id="{{$video->id}}">
                                                                <span class="fa fa-trash"
                                                                      style="position: absolute; margin-top: 3px;padding-left: 72px;"></span>
                                                                <div
                                                                    class="idea-imgs d-flex align-items-center justify-content-center">
                                                                    <div
                                                                        class="embed-responsive embed-responsive-21by9" style="margin-top: 25px">
                                                                        <video class="embed-responsive-item" controls>
                                                                            <source
                                                                                src="{{ asset('videos/'.$video->video_string.'') }}"
                                                                                type="video/mp4">
                                                                            <source
                                                                                src="{{ asset('videos/'.$video->video_string.'') }}"
                                                                                type="video/ogg">
                                                                            Your browser does not support the video tag.
                                                                        </video>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="my-lg-0 mr-3 position-relative">
                                                <button class="btn btn-default my-2 my-sm-0 design-button">
                                                    <span class="design-button-span">Submit</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('js')

    <script type="text/javascript" src="{{ asset('assets/dist/image-uploader.min.js') }}"></script>

    <script>
        $(function () {
            var dataJson = {!! json_encode($startup->galleries,true) !!};

            let preloaded = [];
            if (dataJson.length > 0) {
                dataJson.forEach(function (item, key) {

                    preloaded.push({
                        'id': item['id'],
                        'src': '/images/' + item['image'] + ''
                    })

                });
            }
            // $('.input-images-1').imageUploader({
            //     preloaded: preloaded,
            // });

            $('.input-images-1').imageUploader();

            $(document).on('click touchstart', '.delete-photos', function () {

                form = $(this).closest('form');
                node = $(this);
                var remove_id = $(this).data('remove-id');
                var request = {"remove_id": remove_id};
                if (remove_id !== '') {
                    $.ajax({
                        type: "GET",
                        url: "{{ route('ajax.update.remove.image') }}",
                        data: request,
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            if (response.status == "success") {
                                toastr['success']("Image Deleted Successfully");
                                window.location.reload();
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                }

            });

            $(document).on('click touchstart', '.delete-videos', function () {

                form = $(this).closest('form');
                node = $(this);
                var remove_id = $(this).data('remove-id');
                var request = {"remove_id": remove_id};
                if (remove_id !== '') {
                    $.ajax({
                        type: "GET",
                        url: "{{ route('ajax.update.remove.video') }}",
                        data: request,
                        dataType: "json",
                        cache: true,
                        success: function (response) {
                            if (response.status == "success") {
                                toastr['success']("Video Deleted Successfully");
                                window.location.reload();
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                }

            });


        });
    </script>
@endpush
