@extends('pages.app')
@section('title','Startup Account')
@section('content')
    <section id="startups-information-section">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="section-spacing investor-section-spacing">
                    <div class="container position-relative startups-profile">
                        <div class="row">
                            <a class="add-startup-button" href="{{ route('startup.create') }}">
                                <span class="investors-information-header-icon">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                StartUp</span>
                            </a>
                            <div class="startups-information-wrapper position-relative">
                                <div
                                    class="investors-information-header d-flex align-items-center justify-content-between mb-3" style="">
                                    <span class="investors-information-header-title">Startup Account</span>

                                    <a class="large-screen-edit-btn" href="{{ route('startup.profile',$user->id) }}">
                                        <span class="investors-information-header-icon">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </span>
                                    </a>

                                </div>
                                <div class="investors-information-header d-flex align-items-center justify-content-between mb-3" style="margin-top: 10px;float: right">
                                    <a class="" href="{{ route('cancel.subscription') }}">
                                        <span class="btn btn-danger">Cancel Subscription</span>
                                    </a>
                                </div>
                                <hr>
                                <div class="col-12 startups-imgs-idea p-0">
                                    <div class="row">
                                        <div class="col-12 col-md-5 col-lg-3 idea-imgs-wrapper">
                                            {{-- <div class="idea-imgs">
                                                <img src="../assets/imgs/email.png" class="rounded" alt="...">
                                            </div> --}}
                                            <h5 class="mt-3" class="startup-heading text-muted">Name</h5>
                                            <p>{{$user->name}}</p>
                                            <h5 class="mt-3" class="startup-heading text-muted">Full Name</h5>
                                            <p>{{$user->full_name}}</p>
                                            <h5 class="mt-3" class="startup-heading text-muted">Email</h5>
                                            <p>{{$user->email}}</p>
                                            <h5 class="mt-3" class="startup-heading text-muted">Phone Number</h5>
                                            <p>{{$user->phone_number}}</p>
                                            <h5 class="mt-3" class="startup-heading text-muted">Gender</h5>
                                            <p>{{$user->gender}}</p>

                                        </div>
                                    </div>
                                </div>
                                <div class="startups-information-description">
                                    <h5 class="startup-heading">Description</h5>
                                    <p>{{$user->description}}</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
