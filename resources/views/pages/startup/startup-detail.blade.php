@extends('pages.app')
@section('title','Startup Detail')
@section('content')
    <section id="startups-information-section">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="section-spacing startup-section-spacing">
                    <div class="container">
                        <div class="row">

                            <div class="startups-information-wrapper">
                                <div
                                    class="investors-information-header d-flex align-items-center justify-content-between mb-3">
                                        <span
                                            class="investors-information-header-title">{{$startup->name_startup}}</span>
                                    @if(Auth::user()->user_type == 'startup')
                                        <a href="{{ route('startup.edit',$startup->id) }}"><span
                                                class="investors-information-header-icon"><i
                                                    class="fa fa-pencil" aria-hidden="true"></i></span></a>
                                    @endif
                                    @if(Auth::user()->user_type == 'investor')
                                        <a href="{{ route('page.index',$startup->id) }}"><i
                                                class="startup-chat-wrapper-msg-icon fa fa-comments envelope-icon"
                                                aria-hidden="true"></i></a>
                                    @endif
                                </div>
                                <hr>
                                <div class="startups-information-idea">
                                    <p class="text-muted">{{$startup->branche}}</p>
                                    <h5 class="text text-success">Required Amount ${{$startup->price_startup}}</h5>
                                </div>
                                <div class="col-12 startups-imgs-idea">
                                    <div class="row">
                                        @if(!empty($startup->galleries) && count($startup->galleries) > 0)
                                            @foreach($startup->galleries as $img)
                                                <div
                                                    class="col-12 p-0 p-md-2 col-md-3 mb-4 mb-md-0 idea-imgs-wrapper">
                                                    <div
                                                        class="idea-imgs d-flex align-items-center justify-content-center">
                                                        <img src="{{ asset('images/'.$img->image.'') }}" class="rounded"
                                                             alt="{{$img}}">
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="startups-information-description">
                                    <p>
                                        {{$startup->description_startup}}
                                    </p>
                                </div>
                                <div class="col-12 startups-video-idea">
                                    <div class="row">
                                        @if(!empty($startup->startupVideos) && count($startup->startupVideos) > 0)
                                            @foreach($startup->startupVideos as $vid)
                                                <div class="col-12 col-md-4 mb-4 mb-md-0 idea-video-wrapper">
                                                    <div class="embed-responsive embed-responsive-21by9">
                                                        <video class="embed-responsive-item" controls>
                                                            <source src="{{ asset('videos/'.$vid->video_string.'') }}"
                                                                    type="video/mp4">
                                                            <source src="{{ asset('videos/'.$vid->video_string.'') }}"
                                                                    type="video/ogg">
                                                            Your browser does not support the video tag.
                                                        </video>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
