@extends('pages.app')
@section('title','Startup Account')
@push('css')
    <link rel="stylesheet" href="{{ asset('assets/dist/image-uploader.min.css') }}">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,700|Montserrat:300,400,500,600,700|Source+Code+Pro&display=swap"
          rel="stylesheet">
@endpush
@section('content')

    <section id="startups-information-section">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="section-spacing startup-section-spacing">
                    <div class="container">
                        <div class="row">
                            <div class="startups-information-wrapper">
                                <div
                                    class="investors-information-header d-flex align-items-center justify-content-between mb-3">
                                    <span class="investors-information-header-title">Add Startup Details</span>
                                </div>
                                <hr>
                                <div class="col-12 startups-imgs-idea p-0">
                                    <div class="row">
                                        <form class="col-12 col-sm-5" action="{{ route('startup.store') }}"
                                              method="post" enctype="multipart/form-data" name="form-example-1" id="form-example-1">
                                            @csrf
                                            <div class="form-group mb-4 @error('name_startup') is-invalid @enderror">
                                                <label for="name_startup">Startup Name</label>
                                                <input type="text" class="form-control" id="name_startup"
                                                       name="name_startup" aria-describedby="name_startup"
                                                       value="{{ old('name_startup') }}"
                                                       placeholder="Startup Name" required>
                                                @error('name_startup')
                                                <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-4 @error('branche') is-invalid @enderror">
                                                <label for="branche">Branch</label>
                                                <input type="text" class="form-control" id="branche" name="branche"
                                                       value="{{ old('branche') }}"
                                                       aria-describedby="branche" placeholder="Branche" required>
                                                @error('branche')
                                                <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-4 @error('price_startup') is-invalid @enderror">
                                                <label for="price_startup">Price</label>
                                                <input type="text" class="form-control" id="price_startup"
                                                       value="{{ old('price_startup') }}"
                                                       name="price_startup" aria-describedby="price_startup"
                                                       placeholder="Price" required>
                                                @error('price_startup')
                                                <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div
                                                class="form-group mb-4 @error('description_startup') is-invalid @enderror">
                                                <label for="description_startup">Description</label>
                                                <textarea class="form-control" id="description_startup"
                                                          name="description_startup"
                                                          aria-describedby="description_startup" rows="4"
                                                          required>{{ old('description_startup') }}</textarea>
                                                @error('description_startup')
                                                <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-4 @error('images.*') is-invalid @enderror">
                                                <label for="images">Pictures</label>
                                                <div class="input-images-1" style="padding-top: .5rem;"></div>
{{--                                                <input type="file" class="form-control-file" id="pictures"--}}
{{--                                                       name="pictures[]" multiple--}}
{{--                                                       required>--}}
                                                @error('images.*')
                                                <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group mb-4 @error('pictures.*') is-invalid @enderror">

                                                <div class="user-image mb-3 text-center">
                                                    <div class="imgPreview"></div>
                                                </div>
                                            </div>
                                            <div class="form-group mb-5 @error('videos.*') is-invalid @enderror">
                                                <label for="imgs">Videos</label>
                                                <input type="file" class="form-control-file" id="videos" name="videos[]"
                                                       multiple
                                                >
                                                @error('videos.*')
                                                <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="my-lg-0 mr-3 position-relative">
                                                <button class="btn btn-default my-2 my-sm-0 design-button">
                                                    <span class="design-button-span">Submit</span>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('js')

    <script type="text/javascript" src="{{ asset('assets/dist/image-uploader.min.js') }}"></script>

    <script>
        $(function () {

            $('.input-images-1').imageUploader();


        });
    </script>
@endpush
