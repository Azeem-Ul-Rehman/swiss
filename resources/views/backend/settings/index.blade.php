@extends('layouts.master')
@section('title','Settings')
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ __('Settings') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post"
                              action="{{ route('admin.settings.update', $setting->id) }}"
                              enctype="multipart/form-data" role="form">
                            @csrf
                            @method('patch')
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">


                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="payment_method"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Payment Method') }}</label>

                                            <select id="payment_method"
                                                    class="form-control numberValues @error('payment_method') is-invalid @enderror"
                                                    name="payment_method" autocomplete="payment_method" autofocus>
                                                <option
                                                    value="active" {{ $setting->payment_method == 'active' ? 'selected' : '' }}>
                                                    Active
                                                </option>
                                                <option
                                                    value="inactive" {{ $setting->payment_method == 'inactive' ? 'selected' : '' }}>
                                                    In Active
                                                </option>
                                            </select>

                                            @error('payment_method')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6">
                                            <label for="default_price"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Default Price') }}</label>
                                            <input id="default_price" type="text"
                                                   class="form-control numberValues @error('default_price') is-invalid @enderror"
                                                   name="default_price"
                                                   value="{{ $setting ? $setting->default_price : '' }}"
                                                   autocomplete="tag_line" autofocus>

                                            @error('default_price')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label for="current_price"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Current Price') }}</label>
                                            <input id="current_price" type="text"
                                                   class="form-control @error('current_price') is-invalid @enderror"
                                                   name="current_price"
                                                   value="{{ ($setting) ? $setting->current_price : '' }}"
                                                   autocomplete="current_price" autofocus>

                                            @error('current_price')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="start_date"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Start Date') }}</label>
                                            <input id="start_date" type="date"
                                                   class="form-control @error('start_date') is-invalid @enderror"
                                                   name="start_date"
                                                   value="{{ ($setting) ? $setting->start_date : '' }}"
                                                   autocomplete="start_date" autofocus>

                                            @error('start_date')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="end_date"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('End Date') }}</label>
                                            <input id="end_date" type="date"
                                                   class="form-control @error('end_date') is-invalid @enderror"
                                                   name="end_date"
                                                   value="{{ ($setting) ? $setting->end_date : '' }}"
                                                   autocomplete="end_date" autofocus>

                                            @error('end_date')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $('.numberValues').keypress(function (e) {
            if (isNaN(this.value + "" + String.fromCharCode(e.charCode))) return false;
        }).on("cut copy paste", function (e) {
            e.preventDefault();
        });
    </script>
@endpush
