<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reset Password</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

    <!-- Owl Carousel Min CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"
          integrity="sha512-tS3S5qG0BlhnQROyJXvNjeEM4UpMXHrQfTGmbQ1gKmelCxlSEBUaxhRBj/EFTzpbP4RVSrpEikbmdJobCvhE3g=="
          crossorigin="anonymous"/>
    <!-- Own Carousel Min Theme CSS -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"
          integrity="sha512-sMXtMNL1zRzolHYKEujM2AqCLUR9F2C4/05cdbxjjLSRvMQIciEPCQZo++nk7go3BtSuK9kfa/s+a4f4i5pLkw=="
          crossorigin="anonymous"/>
    <!-- Fontawesome 4.7.0 Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
    <style>
        #our-team-section-count {
            margin-top: -95px;
        }

        .column .card {
            background: #2b3a5e;
        }

        .white-color {
            color: #fff;
        }

        .count-section {
            margin-top: 14px;
            font-size: 35px;
        }

        .text-center {
            text-align: center;
        }

        .privacy {
            padding: 13px;
            color: #fff;
            text-align: center;
        }

        /* Responsive columns */
        @media screen and (max-width: 600px) {
            .column {
                width: 100%;
                display: block;
                margin-bottom: 10px;
            }

            #our-team-section-count {
                margin-top: -41px;
            }
        }
    </style>
</head>
<body>
@if($errors->any())
    <div class="alert alert-danger">
        {{ implode('&', $errors->all(':message!! ')) }}
    </div>
@endif

<header class="m-0">
    <div class="container">
        <div class="row">
            <div class="col-12 p-0">
                <nav class="navbar navbar-light bg-light navbar-expand-md">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>

                    </button>
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                        <a class="navbar-brand" href="{{ route('index') }}"><img
                                src="{{ asset('assets/imgs/main-logo-3.png') }}"
                                width="170" alt="company logo"/></a>
                        <ul class="navbar-nav ml-auto mt-2 mt-md-0 mt-lg-0">
                            <li class="nav-item active">
                                <a class="nav-link px-md-0 px-lg-2" href="{{ route('index') }}">Home <span
                                        class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>

<!-- join us Card Section -->
<section id="startups-information-section">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="section-spacing startup-section-spacing">
                <div class="container">
                    <div class="row">
                        <div class="startups-information-wrapper">
                            <div
                                class="investors-information-header d-flex align-items-center justify-content-between mb-3">
                                <span class="investors-information-header-title">Reset Password</span>
                            </div>
                            <hr>
                            <div class="col-12 startups-imgs-idea p-0">
                                <div class="row">
                                    <form class="col-12 col-sm-5" method="POST" action="/forget-password"
                                          enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group mb-4 @error('email') is-invalid @enderror">
                                            <label for="name_startup">Email:</label>
                                            <input type="text" class="form-control" id="email"
                                                   name="email" aria-describedby="email" value="{{ old('email') }}"
                                                   placeholder="Email" required>
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                         <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                        </div>
                                        <div class="my-lg-0 mr-3 position-relative">
                                            <button class="btn btn-default my-2 my-sm-0 design-button"
                                                    style="width: 100%"><span
                                                    class="design-button-span" style="width: 100%"> Send Password Reset Link</span>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<footer>
    <div class="container-fluid">
        <div class="row m-0">
            <div class="container">
                <div class="row">
                    <div class="footer-section-spacing">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-sm-3">
                                    <p class="footer-title">MIMO FINDS</p>
                                    <div class="footer-img-wrapper">
                                        <a href="{{ route('index') }}">
                                            <img class="img-fluid"
                                                 src="{{ asset('assets/imgs/MimoFinds_Logo_Watersign.png') }}"
                                                 width="70" alt="startup about us img">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-3 mt-4 mt-sm-0">
                                    <p class="footer-title">Links</p>
                                    <ul class="footer-list">
                                        <li><a href="#about-us-section">About us</a></li>
                                        <li><a href="#our-team-section">Our team</a></li>
                                        <li><a href="#contact-us-section">Contact us</a></li>
                                    </ul>
                                </div>
                                @guest
                                    <div class="col-12 col-sm-3 mt-4 mt-sm-0">
                                        <p class="footer-title">Register</p>
                                        <ul class="footer-list">
                                            <li><a href="javascript:void(0);" data-toggle="modal"
                                                   data-target="#startupsRegisterModal">Startup</a></li>
                                            <li><a href="javascript:void(0);" data-toggle="modal"
                                                   data-target="#investorRegisterModal">Investor</a></li>
                                        </ul>
                                    </div>
                                @endguest

                                <div class="col-12 col-sm-3 mt-4 mt-sm-0">
                                    <p class="footer-title">Social</p>
                                    <ul class="footer-list">
                                        <li>
                                            <a href="#" class="mr-1">
                                                    <span class="team-social-icons twitter-icon text-center">
                                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                                    </span>
                                            </a>
                                            <a href="#">
                                                    <span class="team-social-icons linkedin-icon text-center">
                                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                    </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <hr class="footer-hr">

                                <p class="copyrights w-100 d-flex justify-content-between px-3 px-sm-0">

                                    <span> Mimo Finds</span>

                                    <span> All Rights Reserved &copy; <span id="current-year"></span></span>
                                </p>
                                <div class="row w-100">
                                    <div class="col-md-2">

                                    </div>
                                    <div class="col-md-8">
                                        <p class="privacy">
                                            <a href="javascript:void(0)" class="white-color" data-toggle="modal"
                                               data-target="#data-protection">Data Protection</a>
                                            <a href="javascript:void(0)" class="white-color" data-toggle="modal"
                                               data-target="#imprint"> | Imprint</a>
                                            <a href="javascript:void(0)" class="white-color" data-toggle="modal"
                                               data-target="#gtc"> | GTC</a>
                                        </p>

                                    </div>
                                    <div class="col-md-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="{{ asset('js/toastr.min.js') }}"></script>
<script>
    @if(Session::has('flash_message'))
    var type = "{{ Session::get('flash_status') }}";
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('flash_message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('flash_message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('flash_message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('flash_message') }}");
            break;
    }
    @endif

</script>
</body>
</html>
