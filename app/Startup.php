<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Startup extends Model
{
    protected $table = "startups";
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function galleries()
    {
        return $this->hasMany(StartupGallery::class, 'startup_id', 'id');
    }

    public function startupvideos()
    {
        return $this->hasMany(StartupVideo::class, 'startup_id', 'id');
    }
}
