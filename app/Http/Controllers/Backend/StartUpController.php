<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Message;
use App\Startup;
use App\StartupGallery;
use App\StartupVideo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class StartUpController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        $users = User::orderBy('id', 'DESC')->where('user_type', 'startup')->get();
        return view('backend.startups.index', compact('users'));
    }


    public function destroy($id)
    {
        $user = User::findOrFail($id);
        Startup::where('user_id', $id)->delete();
        StartupVideo::where('startup_id', $id)->delete();
        StartupGallery::where('startup_id', $id)->delete();
        Message::where('from', $id)->delete();
        Message::where('to', $id)->delete();
        $user->delete();

        return redirect()->route('admin.startups.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Investor has been Deleted.'
            ]);
    }

}
