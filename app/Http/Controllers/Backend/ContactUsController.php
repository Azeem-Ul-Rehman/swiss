<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Mail\ContactUsEmail;
use App\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public function index()
    {
        $contacts = ContactUs::orderBy('id', 'DESC')->get();
        return view('backend.contacts.index', compact('contacts'));

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'description' => 'required'
        ]);
        $contact = ContactUs::create($request->all());
        $contact_us_email = Mail::to('info@mimofinds.ch')->send(new ContactUsEmail($contact));

        return back()->with([
            'flash_status' => 'success',
            'flash_message' => 'Your message has been sent.'
        ]);
    }

    public function destroy($id)
    {
        $contactus = ContactUs::findOrFail($id);
        $contactus->delete();

        return redirect()->route('admin.contacts.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Contact has been deleted'
            ]);
    }
}
