<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Message;
use App\User;

class InvestorController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        $users = User::orderBy('id', 'DESC')->where('user_type', 'investor')->get();
        return view('backend.investors.index', compact('users'));
    }


    public function destroy($id)
    {
        $user = User::findOrFail($id);
        Message::where('from', $id)->delete();
        Message::where('to', $id)->delete();
        $user->delete();

        return redirect()->route('admin.investors.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Investor has been Deleted.'
            ]);
    }

}
