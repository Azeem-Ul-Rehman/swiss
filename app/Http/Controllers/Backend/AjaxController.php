<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\StartupGallery;
use App\StartupVideo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AjaxController extends Controller
{

    public function deleteStartupImage(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'remove_id' => 'required|exists:startup_galleries,id'
        ], [
            'remove_id.required' => "Image Id is Required.",
            'remove_id.exists' => "Invalid Image Id Selected."
        ]);

        if (!$validator->fails()) {
            StartupGallery::where('id', $request->get('remove_id'))->delete();
            $response['status'] = 'success';
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;

    }


    public function deleteStartupVideo(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'remove_id' => 'required|exists:startup_videos,id'
        ], [
            'remove_id.required' => "Video Id is Required.",
            'remove_id.exists' => "Invalid Image Id Selected."
        ]);

        if (!$validator->fails()) {
            StartupVideo::where('id', $request->get('remove_id'))->delete();
            $response['status'] = 'success';
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;

    }


}
