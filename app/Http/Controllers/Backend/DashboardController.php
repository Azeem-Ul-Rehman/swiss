<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\User;

class DashboardController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        $investor_count = User::where('user_type', 'investor')->get()->count();
        $startup_count = User::where('user_type', 'startup')->get()->count();
        return view('backend.dashboard.index', compact('investor_count', 'startup_count'));
    }
}
