<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $setting = Setting::first();
        return view('backend.settings.index', compact('setting'));
    }


    public function update(Request $request, $id)
    {
        $setting =Setting::find($id);

        $setting->update([
            'payment_method' => $request->payment_method,
            'default_price' => $request->default_price,
            'current_price' => $request->current_price,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
        ]);
        return redirect()->route('admin.settings.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Setting updated successfully.'
            ]);

    }
}
