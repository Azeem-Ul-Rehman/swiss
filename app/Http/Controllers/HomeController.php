<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $userType = $user->user_type;

        if ($userType == 'admin') {
            return redirect()->route('admin.dashboard.index');
        } else {
            $setting = Setting::first();
            if ($setting->payment_method == 'inactive') {
                if ($userType == "startup") {
                    return redirect()->route('startup.index');
                } else {
                    return redirect()->route('investor.index');
                }
            } else {
                return $this->checkPayment($user, $userType);
            }

        }
    }

    public function checkPayment($user, $userType)
    {
        if ($user->status == 'pending') {
            return redirect()->route('payment.index');
        } else {
            $todayDate = now();
            $expiryDate = $user->expiry_date;
            if ($todayDate >= $expiryDate) {
                return redirect()->route('payment.index')->with([
                    'flash_status' => 'success',
                    'flash_message' => 'Your Monthly Subscription is Expired.'
                ]);
            } else {
                if ($userType == "startup") {
                    return redirect()->route('startup.index');
                } else {
                    return redirect()->route('investor.index');

                }
            }

        }
    }
}
