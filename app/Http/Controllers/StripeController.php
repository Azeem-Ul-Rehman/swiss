<?php

namespace App\Http\Controllers;

use App\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

class StripeController extends Controller
{
    private $stripe_confg;

    public function __construct()
    {


        $this->stripe_confg = Config::get('stripe');
    }

    public function stripe()
    {
        if (Auth::check() == false) {
            return redirect()->route('index')->with([
                'flash_status' => 'error',
                'flash_message' => 'You are Unauthorized for this request.Please Login'
            ]);
        } else {
            $setting = Setting::first();
            if (!is_null($setting->current_price)) {
                if (date('Y-m-d') >= $setting->start_date && date('Y-m-d') <= $setting->end_date) {
                    $price = $setting->current_price;
                } else {
                    $price = $setting->default_price;
                }
            } else {
                $price = $setting->default_price;
            }
            return view('pages.stripe', compact('price'));
        }

    }

    public function stripePost(Request $request)
    {

        $this->validate($request, [
            'card_holder_name' => 'required',
            'card-number' => 'required|min:16|max:16',
            'card-cvc' => 'required|min:3|max:3',
            'card-expiry-month' => 'required|min:2|max:2',
            'card-expiry-year' => 'required|min:4|max:4',

        ], [
            'card_holder_name.required' => 'Card Holder Name is required.',
            'card-number.required' => 'Card Number is required.',
            'card-cvc.required' => 'CVC is required.',
            'card-expiry-month.required' => 'Month is required.',
            'card-expiry-year' => 'Year is required.',
        ]);


        try {
            //dd($this->stripe_confg);
            Stripe::setApiKey($this->stripe_confg['stripe_secret']);
            $customer = Customer::create(array(
                'source' => $request->stripeToken,
                'name' => $request->card_holder_name
            ));
            $charge = Charge::create(array(
                'customer' => $customer->id,
                "amount" => $request->amount * 100,
                'currency' => 'CHF',

            ));

            if ($charge->status == 'succeeded') {
                $date = now();
                $user = User::find(Auth::id());
                $user->status = 'active';
                $user->start_date = $date;
                $user->expiry_date = date('Y-m-d', strtotime("+1 months", strtotime($date)));
                $user->save();
                $flash_status = 'success';
                $flash_message = 'Payment Successfull.';

                if (Auth::user()->user_type == 'startup') {
                    return redirect()->route('startup.index')->with([
                        'flash_status' => $flash_status,
                        'flash_message' => $flash_message
                    ]);
                } else {
                    return redirect()->route('investor.index')->with([
                        'flash_status' => $flash_status,
                        'flash_message' => $flash_message
                    ]);
                }


            }

        } catch (\Stripe\Exception\CardException $e) {
            // Since it's a decline, \Stripe\Exception\CardException will be caught

            $flash_status = 'error';
            $flash_message = 'Status is:' . $e->getHttpStatus() . '\n' . 'Type is:' . $e->getError()->type . '\n' . 'Code is:' . $e->getError()->code .
                '\n' . 'Param is:' . $e->getError()->param . '\n' . 'Message is:' . $e->getError()->message . '\n';


        } catch (\Stripe\Exception\RateLimitException $e) {
            // Too many requests made to the API too quickly
            $flash_status = 'error';
            $flash_message = $e->getMessage();

        } catch (\Stripe\Exception\InvalidRequestException $e) {
            // Invalid parameters were supplied to Stripe's API
            $flash_status = 'error';
            $flash_message = $e->getMessage();
        } catch (\Stripe\Exception\AuthenticationException $e) {
            // Authentication with Stripe's API failed
            $flash_status = 'error';
            $flash_message = $e->getMessage();
            // (maybe you changed API keys recently)
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            // Network communication with Stripe failed
            $flash_status = 'error';
            $flash_message = $e->getMessage();
        } catch (\Stripe\Exception\ApiErrorException $e) {
            // Display a very generic error to the user, and maybe send
            $flash_status = 'error';
            $flash_message = $e->getMessage();
            // yourself an email
        } catch (\Exception $ex) {
            $flash_status = 'error';
            $flash_message = $ex->getMessage();
        }
        return back()->with([
            'flash_status' => $flash_status,
            'flash_message' => $flash_message
        ]);


    }
}
