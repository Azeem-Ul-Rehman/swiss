<?php

namespace App\Http\Controllers;

use App\Mail\NotificationSendEmail;
use App\Message;
use App\Startup;
use App\StartupGallery;
use App\StartupVideo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Pusher\Pusher;

class StartUpController extends Controller
{

    public function __construct()
    {
        Auth::user();
    }

    public function index()
    {
        $user = Auth::user();

        if (!is_null($user)) {

            return view('pages.startup.profile', compact('user'));


        } else {
            return redirect()->route('index');
        }
    }

    public function editProfile($id)
    {
        if (Auth::user()) {
            $user = User::find($id);
            if ($user) {

                return view('pages.startup.profile-edit', compact('user'));

            } else {
                return redirect()->route('index');
            }
        } else {
            return redirect()->route('index');
        }
    }

    public function updateProfile(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if (!is_null($user)) {
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone_number = $request->phone_number;
            $user->gender = $request->gender;
            $user->full_name = $request->full_name;
            $user->description = $request->description;
            $user->save();
            return redirect()->route('startup.index')->with([
                'flash_status' => 'success',
                'flash_message' => 'Profile Updated Successfully.'
            ]);
        } else {
            return redirect()->route('index');
        }
    }

    public function create()
    {
        $user = Auth::user();

        if (!is_null($user)) {

            return view('pages.startup.startup-add');


        } else {
            return redirect()->route('index');
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name_startup' => 'required',
            'branche' => 'required',
            'price_startup' => 'required',
            'description_startup' => 'required',
            'images' => 'array',
            'images.*' => 'mimes:jpeg,jpg,png|max:10000',
            "videos" => "array",
            'videos.*' => 'mimes:mp4|max:10000'

        ], [
            'name_startup.required' => 'Name is required.',
            'branche.required' => 'Branch is required.',
            'price_startup.required' => 'Price is required.',
            'description_startup.required' => 'Description is required.',
        ]);

        $user = User::find(Auth::user()->id);
        if (!is_null($user)) {
            $startup = new Startup();
            $startup->name_startup = $request->name_startup;
            $startup->branche = $request->branche;
            $startup->price_startup = $request->price_startup;
            $startup->description_startup = $request->description_startup;
            $startup->user_id = $user->id;


//            if ($request->hasfile('videos')) {
//
//                foreach ($request->file('videos') as $vid) {
//                    $filenameWithExt = $vid->getClientOriginalName();
//                    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
//                    $extension = $vid->getClientOriginalExtension();
//                    $fileNameToStore = $filename . '_' . time() . '.' . $extension;
//                    $vid->move(public_path() . '/videos/', $fileNameToStore);
//                    $videos[] = $fileNameToStore;
//
//                }
//                $startup->videos = json_encode($videos);
//            }

            $startup->save();


            if ($request->hasfile('videos')) {

                foreach ($request->file('videos') as $video) {
                    $var = date_create();
                    $time = date_format($var, 'YmdHis');
                    $name = $time . '-' . $video->getClientOriginalName();
                    $video->move(public_path() . '/videos/', $name);
                    $dataVideos[] = $name;
                    $StartupVideo = new StartupVideo();
                    $StartupVideo->startup_id = $startup->id;
                    $StartupVideo->video_string = $name;
                    $StartupVideo->save();
                }
                $startup->videos = json_encode($dataVideos);
                $startup->save();

            }
            if ($request->hasfile('images')) {

                foreach ($request->file('images') as $image) {
                    $var = date_create();
                    $time = date_format($var, 'YmdHis');
                    $name = $time . '-' . $image->getClientOriginalName();
                    $image->move(public_path() . '/images/', $name);
                    $data[] = $name;
                    $startupGallery = new StartupGallery();
                    $startupGallery->startup_id = $startup->id;
                    $startupGallery->image = $name;
                    $startupGallery->save();
                }
                $startup->pictures = json_encode($data);
                $startup->save();

            }
            return redirect()->route('discover')->with([
                'flash_status' => 'success',
                'flash_message' => 'Startup Created Successfully.'
            ]);
        } else {
            return redirect()->route('index');
        }
    }


    public function discover()
    {

        $user = Auth::user();

        if (!is_null($user)) {

            if ($user->user_type == 'investor') {
                $discover = Startup::with('galleries', 'startupVideos')->get();
                return view('pages.discover', compact('discover'));
            } else {
                $discover = Startup::where('user_id', Auth::id())->with('galleries','startupVideos')->get();
                return view('pages.discover', compact('discover'));
            }


        } else {
            return redirect()->route('index');
        }
    }

    public function startupDetail($id)
    {
        $user = Auth::user();
        if (!is_null($user)) {

            if ($user->user_type == 'investor') {
                $startup = Startup::where('id', $id)->with('galleries','startupVideos')->first();
                return view('pages.startup.startup-detail', compact('startup'));
            } else {
                $startup = Startup::where('id', $id)->where('user_id', Auth::id())->with('galleries','startupVideos')->first();
                return view('pages.startup.startup-detail', compact('startup'));
            }


        } else {
            return redirect()->route('index');
        }
    }


    public function edit($id)
    {
        $user = Auth::user();

        if (!is_null($user)) {

            $startup = Startup::where('id', $id)->with('galleries','startupVideos')->first();
            return view('pages.startup.startup-edit', compact('startup'));


        } else {
            return redirect()->route('index');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name_startup' => 'required',
            'branche' => 'required',
            'price_startup' => 'required',
            'description_startup' => 'required',
            'images' => 'array',
            'images.*' => 'mimes:jpeg,jpg,png|max:10000',
            "videos" => "array",
            'videos.*' => 'mimes:mp4|max:10000'
        ], [
            'name_startup.required' => 'Name is required.',
            'branche.required' => 'Branch is required.',
            'price_startup.required' => 'Price is required.',
            'description_startup.required' => 'Description is required.',
        ]);

        $user = User::find(Auth::user()->id);
        if (!is_null($user)) {
            $startup = Startup::where('id', $request->id)->first();
            $startup->name_startup = $request->name_startup;
            $startup->branche = $request->branche;
            $startup->price_startup = $request->price_startup;
            $startup->description_startup = $request->description_startup;
            $startup->user_id = $user->id;

            if ($request->hasfile('images')) {
                StartupGallery::where('startup_id', $request->id)->delete();
                foreach ($request->file('images') as $image) {
                    $var = date_create();
                    $time = date_format($var, 'YmdHis');
                    $name = $time . '-' . $image->getClientOriginalName();
                    $image->move(public_path() . '/images/', $name);
                    $data[] = $name;
                    $startupGallery = new StartupGallery();
                    $startupGallery->startup_id = $startup->id;
                    $startupGallery->image = $name;
                    $startupGallery->save();
                }
                $startup->pictures = json_encode($data);
                $startup->save();

            }
            if ($request->hasfile('videos')) {

                foreach ($request->file('videos') as $video) {
                    $var = date_create();
                    $time = date_format($var, 'YmdHis');
                    $name = $time . '-' . $video->getClientOriginalName();
                    $video->move(public_path() . '/videos/', $name);
                    $dataVideos[] = $name;
                    $StartupVideo = new StartupVideo();
                    $StartupVideo->startup_id = $startup->id;
                    $StartupVideo->video_string = $name;
                    $StartupVideo->save();
                }
                $startup->videos = json_encode($dataVideos);
                $startup->save();

            }


            $startup->save();
            return redirect()->route('discover')->with([
                'flash_status' => 'success',
                'flash_message' => 'Startup Updated Successfully.'
            ]);
        } else {
            return redirect()->route('index');
        }
    }

    public function destroy($id)
    {
        $user = User::find(Auth::user()->id);
        if (!is_null($user)) {
            $startup = Startup::findOrFail($id);
            StartupVideo::where('startup_id', $id)->delete();
            StartupGallery::where('startup_id', $id)->delete();
            Message::where('from', $id)->delete();
            Message::where('to', $id)->delete();
            $startup->delete();
            return redirect()->route('discover')->with([
                'flash_status' => 'success',
                'flash_message' => 'Startup Deleted Successfully.'
            ]);
        } else {
            return redirect()->route('index');
        }
    }


    public function viewMessage()
    {

//        $users = DB::select("select users.id, users.name, users.email, count(is_read) as unread
//        from users LEFT  JOIN  messages ON users.id = messages.from and is_read = 0 and messages.to = " . Auth::id() . "
//        where user_type='investor'
//        group by users.id, users.name, users.email");
//
//        dd($users);

        $messages = Message::where('to', Auth::id())->groupBy('discover_id')->get();

        foreach ($messages as $key => $message) {
            $messages[$key]->unread = Message::where('to', $message->to)->where('discover_id', $message->discover_id)->where('is_read', 0)->count();
            $messages[$key]->startup = Startup::where('id', $message->discover_id)->first();
            $messages[$key]->user = User::where('id', $message->from)->first();
        }

//        $users = $message;
//        dd($messages);
//        if (!empty($fromIds) && count($fromIds) > 0) {
//            $users = User::whereHas('startups',function ($q) use ($fromIds){
//                $q->whereIn('id',$fromIds);
//            })->get();
//            dd($users);
//            foreach ($users as $key => $user) {
//                $users[$key]->unread = Message::where('from', $user->id)->where('is_read', 0)->count();
//            }
//        } else {
//            $users = null;
//        }

        $user = Auth::user();

        return view('pages.startup.messages.index', compact('messages'));


    }

    public function getMessage(Request $request)
    {
        $my_id = Auth::id();

        // Make read all unread message
        Message::where(['from' => $request->id, 'to' => $my_id, 'discover_id' => $request->discover_id])->update(['is_read' => 1]);

        // Get all message from selected user
        $messages = Message::where(function ($query) use ($request, $my_id) {
            $query->where('from', $request->id)->where('to', $my_id)->where('discover_id', $request->discover_id);
        })->orWhere(function ($query) use ($request, $my_id) {
            $query->where('from', $my_id)->where('to', $request->id)->where('discover_id', $request->discover_id);
        })->orderBy('created_at', 'asc')->get();

        $currentUser = User::where('id', $request->id)->first();

        return view('pages.startup.messages.message', compact('messages', 'currentUser'));
    }

    public function sendMessage(Request $request)
    {
        $from = Auth::id();
        $to = $request->receiver_id;
        $message = $request->message;

        $data = new Message();
        $data->from = $from;
        $data->to = $to;
        $data->message = $message;
        $data->discover_id = $request->discover_id;
        $data->type = 'guider';
        $data->is_read = 0; // message will be unread when sending message
        $data->save();


        $messages = Message::where('from', $from)->where('discover_id', $request->discover_id)->get();

        if (count($messages) == 1) {
            $fromUser = User::where('id', $from)->first();
            $toUser = User::where('id', $to)->first();
            Mail::to($toUser->email)->send(new NotificationSendEmail($fromUser, $toUser));
        }


        // pusher
        $options = array(
            'cluster' => 'ap2',
            // 'useTLS' => true
        );


        $pusher = new Pusher(
            '0ea14e6e7c66a09c4a7d',
            '94f08b9c86fed7b00b8e',
            '964717',
            $options
        );


        $data = ['from' => $from, 'to' => $to, 'discover_id' => $request->discover_id]; // sending from and to user id when pressed enter
        $pusher->trigger('my-channel', 'my-event', $data);


    }

    public function cancelSubscription()
    {
        $user = User::find(Auth::id());
        if (!is_null($user)) {
            $user->start_date = null;
            $user->expiry_date = null;
            $user->save();

            return redirect()->route('payment.index')->with([
                'flash_status' => 'success',
                'flash_message' => 'Subscription Cancel Successfully.'
            ]);
        } else {
            return redirect()->route('index');
        }
    }
}
