<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Setting;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, $user)
    {
        $userType = $user->user_type;

        if ($userType == 'admin') {
            return redirect()->route('admin.dashboard.index');
        } else {
            $setting = Setting::first();
            if ($setting->payment_method == 'inactive') {
                if ($userType == "startup") {
                    return redirect()->route('startup.index');
                } else {
                    return redirect()->route('investor.index');
                }
            } else {
                return $this->checkPayment($user, $userType);
            }

        }


    }

    public function checkPayment($user, $userType)
    {
        if ($user->status == 'pending') {
            return redirect()->route('payment.index');
        } else {
            $todayDate = now();
            $expiryDate = $user->expiry_date;
            if ($todayDate >= $expiryDate) {
                return redirect()->route('payment.index')->with([
                    'flash_status' => 'success',
                    'flash_message' => 'Your Monthly Subscription is Expired.'
                ]);
            } else {
                if ($userType == "startup") {
                    return redirect()->route('startup.index');
                } else {
                    return redirect()->route('investor.index');

                }
            }

        }
    }
}
