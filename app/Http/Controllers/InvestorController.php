<?php

namespace App\Http\Controllers;

use App\Mail\NotificationSendEmail;
use App\Message;
use App\Startup;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Pusher\Pusher;
use Pusher\PusherException;

class InvestorController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        if (!is_null($user)) {

            return view('pages.investor', compact('user'));
        } else {
            return redirect()->route('index');
        }


    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        if (Auth::user()) {
            $user = User::find($id);
            if ($user) {

                return view('pages.investor-add', compact('user'));

            } else {
                return redirect()->route('index');
            }
        } else {
            return redirect()->route('index');
        }
    }

    public function update(Request $request)
    {
        $user = User::find(Auth::user()->id);
        if (!is_null($user)) {
            $user->name = $request->name;
            $user->email = $request->email;
            $user->phone_number = $request->phone_number;
            $user->gender = $request->gender;
            $user->full_name = $request->full_name;
            $user->description = $request->description;
            $user->save();
            return redirect()->route('investor.index')->with([
                'flash_status' => 'success',
                'flash_message' => 'Profile Updated Successfully.'
            ]);
        } else {
            return redirect()->route('index');
        }
    }

    public function destroy($id)
    {
        //
    }

    public function viewMessage($id)
    {
//        $users = User::all();
        // count how many message are unread from the selected user
//        $users = DB::select("select users.id, users.name, users.avatar, users.email, count(is_read) as unread
//        from users LEFT  JOIN  messages ON users.id = messages.from and is_read = 0 and messages.to = " . $id . "
//        where users.id != " . $id . "
//        group by users.id, users.name, users.avatar, users.email");

        $users = DB::select("select users.id, users.name, users.email, count(is_read) as unread
        from users LEFT JOIN  messages ON users.id = messages.from and is_read = 0 and messages.to = " . Auth::id() . "
        group by users.id, users.name, users.email");


        $message = Message::where('from', Auth::id())->where('type', 'customer')->groupBy('to')->pluck('to')->toArray();


//        $students = \DB::table('users')
//            ->select(
//                'users.id',
//                'users.name'
//            )->whereExists( function ($query)  {
//                $query->select(DB::raw(1))
//                    ->from('messages')
//                    ->whereRaw('users.id = messages.to');
//
//            })->get();

//        dd($students);


        $allUsers = User::pluck('id')->toArray();
        $getMessage = \App\Message::whereIn('to', $allUsers)->get();

        $discover_id = $id;
        $usedId = Startup::where('id', $id)->first()->user_id;
        $id = $usedId;

        $user = Auth::user();

        return view('pages.investor.messages.index', compact('users', 'id', 'getMessage', 'discover_id'));


    }

    public function getMessage(Request $request)
    {
        $my_id = Auth::id();

        // Make read all unread message
        Message::where(['from' => $request->id, 'to' => $my_id, 'discover_id' => $request->discover_id])->update(['is_read' => 1]);

        // Get all message from selected user
        $messages = Message::where(function ($query) use ($request, $my_id) {
            $query->where('from', $request->id)->where('to', $my_id)->where('discover_id', $request->discover_id);
        })->orWhere(function ($query) use ($request, $my_id) {
            $query->where('from', $my_id)->where('to', $request->id)->where('discover_id', $request->discover_id);
        })->orderBy('created_at', 'asc')->get();

        $currentUser = User::where('id', $request->id)->first();


        return view('pages.investor.messages.message', compact('messages', 'currentUser'));
    }

    public function sendMessage(Request $request)
    {

        $from = Auth::id();
        $to = $request->receiver_id;
        $message = $request->message;

        $data = new Message();
        $data->from = $from;
        $data->to = $to;
        $data->message = $message;
        $data->discover_id = $request->discover_id;
        $data->type = 'customer';
        $data->is_read = false; // message will be unread when sending message
        $data->save();

        $messagesCount = Message::where('from', $from)->where('discover_id', $request->discover_id)->get();
        if (count($messagesCount) == 1) {

            $fromUser = User::where('id', $from)->first();
            $toUser = User::where('id', $to)->first();

            Mail::to($toUser->email)->send(new NotificationSendEmail($fromUser, $toUser));
        }

        // pusher
        $options = array(
            'cluster' => 'ap2',
            // 'useTLS' => true
        );

        $pusher = new Pusher(
            '0ea14e6e7c66a09c4a7d',
            '94f08b9c86fed7b00b8e',
            '964717',
            $options
        );


        $data = ['from' => $from, 'to' => $to, 'discover_id' => $request->discover_id]; // sending from and to user id when pressed enter
        $pusher->trigger('my-channel', 'my-event', $data);


    }

    public function getAllMessage()
    {

//        $users = DB::select("select users.id, users.name, users.email, count(is_read) as unread
//        from users LEFT  JOIN  messages ON users.id = messages.from and is_read = 0 and messages.to = " . Auth::id() . "
//        where user_type='investor'
//        group by users.id, users.name, users.email");
//
//        dd($users);

        $messages = Message::where('from', Auth::id())->groupBy('discover_id')->get();


        foreach ($messages as $key => $message) {
            $messages[$key]->unread = Message::where('to', Auth::id())->where('discover_id', $message->discover_id)->where('is_read', 0)->count();
            $messages[$key]->startup = Startup::where('id', $message->discover_id)->first();
            $messages[$key]->user = User::where('id', $message->to)->first();
        }

        $user = Auth::user();

        return view('pages.investor.messages.global-inbox', compact('messages'));


    }
}
