<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificationSendEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $fromUser;
    public $toUser;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($fromUser, $toUser)
    {
        $this->fromUser = $fromUser;
        $this->toUser = $toUser;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from($this->fromUser->email)->subject('Notification')->view('mails.notification');
    }
}
