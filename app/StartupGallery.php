<?php

namespace App;

use App\Startup;
use Illuminate\Database\Eloquent\Model;

class StartupGallery extends Model
{
    protected $table = 'startup_galleries';
    protected $guarded = [];


    public function event()
    {
        return $this->belongsTo(Startup::class, 'startup_id', 'id');
    }

    public function getImagePathAttribute()
    {
        return asset('images/startup/' . $this->image);
    }
}
