<?php

namespace App;

use App\Startup;
use Illuminate\Database\Eloquent\Model;

class StartupVideo extends Model
{
    protected $table = 'startup_videos';
    protected $guarded = [];


    public function event()
    {
        return $this->belongsTo(Startup::class, 'startup_id', 'id');
    }

}
